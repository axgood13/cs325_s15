package assignments.clean;

import static org.junit.Assert.*;

import org.junit.Test;

public class VisitorTest {
	public CleaningVisitor cleaner = new CleaningVisitor();
	public PartyPrepVisitor partyPrepper = new PartyPrepVisitor();
	public Bathroom bathroom = new Bathroom();
	public Kitchen kitchen = new Kitchen();
	public LivingRoom livingRoom = new LivingRoom();
	public Den den = new Den();
	
	@Test
	public void testCleanBathroom() {
		assertEquals("Scrub floors.", bathroom.accept(cleaner));
	}
	
	@Test
	public void testPartyPrepBathroom() {
		assertEquals("Clean towels and get fresh TP.", bathroom.accept(partyPrepper));
	}
	
	@Test
	public void testCleanLivingRoom() {
		assertEquals("Vacuum carpet.", livingRoom.accept(cleaner));
	}
	
	@Test
	public void testPartyPrepLivingRoom() {
		assertEquals("Prepare drinks.", livingRoom.accept(partyPrepper));
	}
	
	@Test
	public void testCleanKitchen() {
		assertEquals("Wax floors.", kitchen.accept(cleaner));
	}
	
	@Test
	public void testPartyPrepKitchen() {
		assertEquals("Prepare buffet.", kitchen.accept(partyPrepper));
	}
	
	@Test
	public void testCleanDen() {
		assertEquals("Sweep floors.", den.accept(cleaner));
	}
	
	@Test
	public void testPartyPrepDen() {
		assertEquals("Put out chips and nuts.", den.accept(partyPrepper));
	}

}
