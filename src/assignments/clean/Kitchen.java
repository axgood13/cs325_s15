package assignments.clean;

public class Kitchen implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.visitKitchen(this);
	}

	@Override
	public String operationPartyPrep() {
		return "Prepare buffet.";
	}

	@Override
	public String operationClean() {
		return "Wax floors.";
	}

}
