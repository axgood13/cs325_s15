package assignments.clean;

public class Den implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.visitDen(this);
	}

	@Override
	public String operationPartyPrep() {
		return "Put out chips and nuts.";
	}

	@Override
	public String operationClean() {
		return "Sweep floors.";
	}

}
