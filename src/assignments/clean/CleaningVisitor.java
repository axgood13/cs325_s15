package assignments.clean;

public class CleaningVisitor implements Visitor {

	@Override
	public String visitBathroom(Room room) {
		return room.operationClean();
	}

	@Override
	public String visitKitchen(Room room) {
		return room.operationClean();
	}

	@Override
	public String visitLivingRoom(Room room) {
		return room.operationClean();
	}

	@Override
	public String visitDen(Room room) {
		return room.operationClean();
	}

}
