package assignments.clean;

public class Bathroom implements Room {
	
	
	@Override
	public String accept(Visitor visitor) {
		return visitor.visitBathroom(this);
	}

	@Override
	public String operationPartyPrep() {
		return "Clean towels and get fresh TP.";
	}

	@Override
	public String operationClean() {
		return "Scrub floors.";
	}
	
}
