package assignments.clean;

public class PartyPrepVisitor implements Visitor{

	@Override
	public String visitBathroom(Room room) {
		return room.operationPartyPrep();
	}

	@Override
	public String visitKitchen(Room room) {
		return room.operationPartyPrep();
	}

	@Override
	public String visitLivingRoom(Room room) {
		return room.operationPartyPrep();
	}

	@Override
	public String visitDen(Room room) {
		return room.operationPartyPrep();
	}
}
