package assignments.clean;

public class LivingRoom implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.visitLivingRoom(this);
	}

	@Override
	public String operationPartyPrep() {
		return "Prepare drinks.";
	}

	@Override
	public String operationClean() {
		return "Vacuum carpet.";
	}

}
