package assignments.clean;

public interface Visitor {
	public String visitBathroom(Room room);
	public String visitKitchen(Room room);
	public String visitLivingRoom(Room room);
	public String visitDen(Room room);
}
