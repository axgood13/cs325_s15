package assignments.clean;

public interface Room {
	public String accept(Visitor visitor);
	public String operationPartyPrep();
	public String operationClean();
}
