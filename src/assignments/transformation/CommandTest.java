package assignments.transformation;

import static org.junit.Assert.*;

import org.junit.Test;

import assignments.transformation.Command.Context;
import assignments.transformation.Command.Type;

public class CommandTest {

	@Test
	public void testContext() {
		Command command = new Uppercase();
		Context context = command.new Context("a","b","c");
		assertTrue(context.type == Type.ARRAY);
	}

}
