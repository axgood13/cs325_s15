package assignments.transformation;

import java.util.ArrayList;

public class Split extends Command {
	@Override
	public String[] execute(Context context) {
		String[] contextStringArray = context.array;
		ArrayList<String> arraylist = new ArrayList<String>();
		String regex = contextStringArray[contextStringArray.length-1];
		for (int i = 0; i < contextStringArray.length-1; i++) {
			String[] splitString = contextStringArray[i].split(regex);
			for (String string : splitString) {
				arraylist.add(string);
			}
		}
		String[] newString = new String[arraylist.size()];
		for (int i = 0; i < arraylist.size(); i++) {
			newString[i] = arraylist.get(i);
		}
		return newString;
	}

	@Override
	public Command inverse() {
		return new Join();
	}
	
	public String toString() {
		return "Split";
	}
}
