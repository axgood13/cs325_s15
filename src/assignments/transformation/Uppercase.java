package assignments.transformation;

public class Uppercase extends Command{
	@Override
	public String[] execute(Context context) {
		String[] contextStringArray = context.array;
		for (int i = 0; i < contextStringArray.length; i++) {
			contextStringArray[i] = contextStringArray[i].toUpperCase();
		}
		return contextStringArray;
	}

	@Override
	public Command inverse() {
		return new Lowercase();
	}
	
	public String toString() {
		return "Uppercase";
	}
}
