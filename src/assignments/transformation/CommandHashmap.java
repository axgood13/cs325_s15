package assignments.transformation;

import java.util.HashMap;

public class CommandHashmap {
	public static HashMap<String, Class> map = new HashMap<String, Class>(){{
		put("Uppercase", Uppercase.class);	put("uppercase", Uppercase.class);	put("UPPERCASE", Uppercase.class);
		put("Lowercase", Lowercase.class);	put("lowercase", Lowercase.class);	put("LOWERCASE", Lowercase.class);
		put("Reverse", Reverse.class);		put("reverse", Reverse.class);		put("REVERSE", Reverse.class);
		put("Join", Join.class);			put("join", Join.class);			put("JOIN", Join.class);
		put("Split", Split.class);			put("split", Split.class);			put("SPLIT", Split.class);
		put("Undo", Undo.class);			put("undo", Undo.class);			put("UNDO", Undo.class);
	}};
}
