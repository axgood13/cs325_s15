package assignments.transformation;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import assignments.transformation.Command.Type;

public abstract class Command {
	public static Stack<String> stackOfCommands = new Stack<String>();
	private Command next;
	protected abstract String[] execute(Context context);
	protected abstract Command inverse();
	
	enum Type {
		STRING,ARRAY
	}
	
	public class Context {
		Type type;
		String[] array;
		public Context(String... string) {
			this.array = string;
			if (array.length > 1) {
				this.type = Type.ARRAY;
			}
			else if (array.length == 1){
				this.type = Type.STRING;
			}
			else {
				System.out.println("Some sort of error.");
			}
		}
		
		public void updateType() {
			if (array.length > 1) {
				this.type = Type.ARRAY;
			}
			else if (array.length == 1){
				this.type = Type.STRING;
			}
		}
		
		public String toString() {
			if (type == Type.STRING) {
				return array[0];
			}
			else {
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < array.length; i++) {
					if (array[i].length() > 0) {
						String element = array[i];
						stringBuilder.append("[" + "\"" + element + "\"" + "]");
					}
				}
				return stringBuilder.toString();
			}
		}
	}
	
	public Context transformString(String[] transformation, String... stringArray) {
		Class command = CommandHashmap.map.get(transformation[0]);
		Context context = new Context(stringArray);
		if (this.getClass() == command) {
			if (transformation.length == 2 && (command == Split.class || command == Join.class)) {
				String[] tempNewStringArray = new String [stringArray.length+1];
				for (int i = 0; i < stringArray.length; i++) {
					tempNewStringArray[i] = stringArray[i];
				}
				tempNewStringArray[tempNewStringArray.length - 1] 
						= transformation[1];
				stringArray = tempNewStringArray;
			}
			stackOfCommands.push(context.toString());
			context = new Context(stringArray);
			context.array = execute(context);
			context.updateType();
			System.out.println(context.toString());
			return context;
		}
		else if (next != null) {
			return next.transformString(transformation, stringArray);
		}
		System.out.println("Command Not Recognized. Try Again.");
		return context;
	}
	
	private void setNext(Command command) {
		next = command;
	}
	
	public static Command createChain() {
		Command command = new Join();
		
		Command command1 = new Lowercase();
		command.setNext(command1);
		
		Command command2 = new Split();
		command1.setNext(command2);
		
		Command command3 = new Uppercase();
		command2.setNext(command3);
		
		Command command4 = new Reverse();
		command3.setNext(command4);
		
		Command command5 = new Undo();
		command4.setNext(command5);

		return command;
	}
}
