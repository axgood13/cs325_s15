package assignments.transformation;

public class Lowercase extends Command {
	@Override
	public String[] execute(Context context) {
		String[] contextString = context.array;
		for (int i = 0; i < contextString.length; i++) {
			contextString[i] = contextString[i].toLowerCase();
		}
		return contextString;
	}

	@Override
	public Command inverse() {
		return new Uppercase();
	}
	
	public String toString() {
		return "Lowercase";
	}
}
