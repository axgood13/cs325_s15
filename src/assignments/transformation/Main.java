package assignments.transformation;

import java.util.Scanner;

import assignments.transformation.Command.Context;

public class Main {
	public static void main(String [] args) {
		Command chain = Command.createChain();
		Scanner scanner = new Scanner(System.in);
		String[] transformation;
		
		System.out.println("Enter a String: ");
		Context context = chain.new Context(scanner.nextLine());
		
		System.out.println("Enter a Transformation: ");
		transformation = scanner.nextLine().split(" ");
		
		while (transformation[0] != "quit" || transformation[0] != "Quit" || transformation[0] != "QUIT") {
			System.out.println();
			context = chain.transformString(transformation, context.array);
			System.out.println("Enter a Transformation: ");
			transformation = scanner.nextLine().split(" ");
		}
	}
}
