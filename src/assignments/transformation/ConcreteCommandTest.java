package assignments.transformation;

import static org.junit.Assert.*;

import org.junit.Test;

import assignments.transformation.Command.Context;
import assignments.transformation.Command.Type;

public class ConcreteCommandTest {
	Command uppercase = new Uppercase();
	Command lowercase = new Lowercase();
	Command split = new Split();
	Command join = new Join();
	Command reverse = new Reverse();

	@Test
	public void upperCommandReturnsAllCaps() {
		Context context = uppercase.new Context("I am a Partially capitalized PhrasE.");
		assertEquals("I AM A PARTIALLY CAPITALIZED PHRASE.",uppercase.execute(context)[0]);
	}
	
	@Test
	public void upperInverseIsLower() {
		assertTrue(uppercase.inverse() instanceof Lowercase);
	}
	
	@Test
	public void lowerCommandReturnsAllLower() {
		Context context = uppercase.new Context("I am a Partially capitalized PhrasE.");
		assertEquals("i am a partially capitalized phrase.",lowercase.execute(context)[0]);
	}
	
	@Test
	public void lowerInverseIsUpper() {
		assertTrue(lowercase.inverse() instanceof Uppercase);
	}

	@Test
	public void splitCommandReturnsPhraseWithoutSplitChar() {
		Context context = uppercase.new Context("I am a Partially capitalized PhrasE.","a");
//		assertEquals("[\"I \"][\"m \"][\" P\"][\"rti\"][\"lly c\"][\"pit\"][\"lized Phr\"][\"sE.\"]",split.execute(context));
		assertEquals(String[].class,split.execute(context).getClass());
	}
	
	@Test
	public void splitInverseIsJoin() {
		assertTrue(split.inverse() instanceof Join);
	}
	
	@Test
	public void joinCommandReturnsJoinedStrings() {
		Context context = uppercase.new Context("I", "am", "a", "Partially", "capitalized", "PhrasE.", " ");
		assertEquals("I am a Partially capitalized PhrasE.",join.execute(context)[0]);
	}
	
	@Test
	public void joinInverseIsSplit() {
		assertTrue(join.inverse() instanceof Split);
	}
	
	@Test
	public void ReverseCommandReturnsStringInReverse() {
		Context context = uppercase.new Context("I am a Partially capitalized PhrasE.");
		assertEquals(".EsarhP dezilatipac yllaitraP a ma I",reverse.execute(context)[0]);
	}
	
	@Test
	public void reverseInverseIsItself() {
		assertTrue(reverse.inverse() instanceof Reverse);
	}	
}
