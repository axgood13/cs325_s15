package assignments.transformation;

public class Join extends Command {
	@Override
	public String[] execute(Context context) {
		String[] contextStringArray = context.array;
		if (contextStringArray.length < 2) {
			System.out.println("Join Command Requires An Argument. Try Again.");
			return contextStringArray;
		}
		String joinString = contextStringArray[contextStringArray.length - 1];
		StringBuilder builder = new StringBuilder();
		if (contextStringArray.length > 1) {
			for (int i = 0; i < contextStringArray.length - 2; i++) {
				builder.append(contextStringArray[i]).append(joinString);
			}
			builder.append(contextStringArray[contextStringArray.length-2]);
		}
		return new String[] {builder.toString()};
	}

	@Override
	public Command inverse() {
		return new Split();
	}
	
	public String toString() {
		return "Join";
	}
}
