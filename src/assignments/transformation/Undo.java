package assignments.transformation;

public class Undo extends Command {

	@Override
	protected String[] execute(Context context) {
		String[] contextStringArray = context.array;
		try {
			if (stackOfCommands.size() > 1) {
				stackOfCommands.pop();
			}
			String[] oneStringArray = {stackOfCommands.pop().toString()};
			return oneStringArray;
		} catch (Exception e) {
			System.out.println("Can't undo anymore!");
		}
		return contextStringArray;
	}

	@Override
	protected Command inverse() {
		return null;
	}
	
	public String toString() {
		return "Undo";
	}

}
