package assignments.transformation;

public class Reverse extends Command{
	@Override
	public String[] execute(Context context) {
		String[] contextStringArray = context.array;
		for (int i = 0; i < contextStringArray.length; i++) {
			String contextString = contextStringArray[i];
			StringBuilder newString = new StringBuilder();
			for (int j = contextString.length() - 1; j >= 0; j--) {
				newString.append(contextString.charAt(j));
			}
			contextStringArray[i] = newString.toString();
		}
		
		return contextStringArray;
	}

	@Override
	public Command inverse() {
		return new Reverse();
	}
	
	public String toString() {
		return "Reverse";
	}
}
