package assignments.farmerFinal.conventionalOps;

import assignments.farmerFinal.core.Operations;

public class ConventionalFall implements Operations{

	@Override
	public void plow() {
		System.out.println("Conventional Farmer's Fall plowing: no action");
	}

	@Override
	public void plant() {
		System.out.println("Conventional Farmer's Fall planting: no action");
	}

	@Override
	public void harvest() {
		System.out.println("Conventional Farmer's Fall harvest: no action");
	}

	@Override
	public void weedControl() {
		System.out.println("Conventional Farmer's Fall weed control: hire combine");
	}

	@Override
	public void market() {
		System.out.println("Conventional Farmer's Fall marketing: feed corn to elevator");
	}

}
