package assignments.farmerFinal.conventionalOps;

import assignments.farmerFinal.core.Operations;

public class ConventionalSpring implements Operations{

	@Override
	public void plow() {
		System.out.println("Conventional Farmer's Spring plowing: using no-till; no plowing");
	}

	@Override
	public void plant() {
		System.out.println("Conventional Farmer's Spring planting: no action");
	}

	@Override
	public void harvest() {
		System.out.println("Conventional Farmer's Spring harvest: nothing to harvest");
	}

	@Override
	public void weedControl() {
		System.out.println("Conventional Farmer's Spring weed control: no action");
	}

	@Override
	public void market() {
		System.out.println("Conventional Farmer's Spring marketing: nothing to market");
	}

}
