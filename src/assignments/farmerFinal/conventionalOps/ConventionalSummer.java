package assignments.farmerFinal.conventionalOps;

import assignments.farmerFinal.core.Operations;

public class ConventionalSummer implements Operations{

	@Override
	public void plow() {
		System.out.println("Conventional Farmer's Summer plowing: no action");
	}

	@Override
	public void plant() {
		System.out.println("Conventional Farmer's Summer planting: corn");
	}

	@Override
	public void harvest() {
		System.out.println("Conventional Farmer's Summer harvest: nothing to harvest");
	}

	@Override
	public void weedControl() {
		System.out.println("Conventional Farmer's Summer weed control: spray");
	}

	@Override
	public void market() {
		System.out.println("Conventional Farmer's Summer marketing: nothing to market");
	}

}
