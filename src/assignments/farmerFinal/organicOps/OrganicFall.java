package assignments.farmerFinal.organicOps;

import assignments.farmerFinal.core.Operations;

public class OrganicFall implements Operations{

	@Override
	public void plow() {
		System.out.println("Organic Farmer's Fall plowing: no action");
	}

	@Override
	public void plant() {
		System.out.println("Organic Farmer's Fall planting: late beans, squash, potatoes, leafy greens");
	}

	@Override
	public void harvest() {
		System.out.println("Organic Farmer's Fall harvest: employ lots of interns and volunteers; u-pick for squash");
	}

	@Override
	public void weedControl() {
		System.out.println("Organic Farmer's Fall weed control: no action");
	}

	@Override
	public void market() {
		System.out.println("Organic Farmer's Fall marketing: beans, squash, tomatoes to farmer's market; big harvest party on farm");
	}

}
