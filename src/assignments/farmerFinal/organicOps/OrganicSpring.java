package assignments.farmerFinal.organicOps;

import assignments.farmerFinal.core.Operations;

public class OrganicSpring implements Operations{

	@Override
	public void plow() {
		System.out.println("Organic Farmer's Spring plowing: plow under green manure");
	}

	@Override
	public void plant() {
		System.out.println("Organic Farmer's Spring planting: pease, lettuce");
	}

	@Override
	public void harvest() {
		System.out.println("Organic Farmer's Spring harvest: employ lots of interns and volunteers");
	}

	@Override
	public void weedControl() {
		System.out.println("Organic Farmer's Spring weed control: employ lots of interns with hoes");
	}

	@Override
	public void market() {
		System.out.println("Organic Farmer's Spring marketing: fall garlic, peas & lettuce to farmer's market");
	}

}
