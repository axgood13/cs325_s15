package assignments.farmerFinal.organicOps;

import assignments.farmerFinal.core.Operations;

public class OrganicSummer implements Operations{

	@Override
	public void plow() {
		System.out.println("Organic Farmer's Summer plowing: plow fallow fields to prepare for fall cover crop");
	}

	@Override
	public void plant() {
		System.out.println("Organic Farmer's Summer planting: beans, squash, tomatoes, carrots, melons, 2nd round of peas and leafy greens");
	}

	@Override
	public void harvest() {
		System.out.println("Organic Farmer's Summer harvest: employ lots of interns and volunteers");
	}

	@Override
	public void weedControl() {
		System.out.println("Organic Farmer's Summer weed control: employ lots of interns with hoes");
	}

	@Override
	public void market() {
		System.out.println("Organic Farmer's Summer marketing: peas, carrots, early beans, roma tomatoes to farmer's market");
	}

}
