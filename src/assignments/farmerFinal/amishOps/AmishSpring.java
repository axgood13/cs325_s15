package assignments.farmerFinal.amishOps;

import assignments.farmerFinal.core.Operations;

public class AmishSpring implements Operations{

	@Override
	public void plow() {
		System.out.println("Amish Farmer's Spring plowing: plow corn fields.");
	}

	@Override
	public void plant() {
		System.out.println("Amish Farmer's Spring planting: peas, lettuce, oats");
	}

	@Override
	public void harvest() {
		System.out.println("Amish Farmer's Spring harvest: wife and kids help out");
	}

	@Override
	public void weedControl() {
		System.out.println("Amish Farmer's Spring weed control: walking cultivator in garden");
	}

	@Override
	public void market() {
		System.out.println("Amish Farmer's Spring marketing: jams, jellies, peas & lettuce");
	}

}
