package assignments.farmerFinal.amishOps;

import assignments.farmerFinal.core.Operations;

public class AmishSummer implements Operations{

	@Override
	public void plow() {
		System.out.println("Amish Farmer's Summer plowing: no action");
	}

	@Override
	public void plant() {
		System.out.println("Amish Farmer's Summer planting: beans, squash, tomatoes, beets, carrots");
	}

	@Override
	public void harvest() {
		System.out.println("Amish Farmer's Summer harvest: wife and kids help out in garden; neighbors help with oats and hay");
	}

	@Override
	public void weedControl() {
		System.out.println("Amish Farmer's Summer weed control: walking cultivator in garden, two-row, horse-drawn cultivator in fields");
	}

	@Override
	public void market() {
		System.out.println("Amish Farmer's Summer marketing: peas, carrots, early beans, roma tomatoes to auction");
	}

}
