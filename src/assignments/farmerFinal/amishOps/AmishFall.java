package assignments.farmerFinal.amishOps;

import assignments.farmerFinal.core.Operations;

public class AmishFall implements Operations{

	@Override
	public void plow() {
		System.out.println("Amish Farmer's Fall plowing: no action");
	}

	@Override
	public void plant() {
		System.out.println("Amish Farmer's Fall planting: late beans, squash, potatoes");
	}

	@Override
	public void harvest() {
		System.out.println("Amish Farmer's Fall harvest: wife and kids help out in garden, neighbors help with corn and hay");
	}

	@Override
	public void weedControl() {
		System.out.println("Amish Farmer's Fall weed control: no action");
	}

	@Override
	public void market() {
		System.out.println("Amish Farmer's Fall marketing: beans, squash, tomatoes to auction");
	}

}
