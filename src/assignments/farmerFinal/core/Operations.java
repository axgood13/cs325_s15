package assignments.farmerFinal.core;

public interface Operations {
	public void plow();
	public void plant();
	public void harvest();
	public void weedControl();
	public void market();
}
