package assignments.farmerFinal.core;

import java.util.HashMap;

public class Farmer {
	enum Season {
		SPRING,SUMMER,FALL
	}
	
	private Operations currentOps;
	private HashMap<Season, Operations> map = new HashMap<Season, Operations>();
	
	public Farmer(Operations springOps, Operations summerOps, Operations fallOps) {
		map.put(Season.SPRING, springOps);
		map.put(Season.SUMMER, summerOps);
		map.put(Season.FALL, fallOps);
	}
	
	public void plow() {
		currentOps.plow();
	}
	
	public void plant() {
		currentOps.plant();
	}
	
	public void weedControl() {
		currentOps.weedControl();
	}
	
	public void harvest() {
		currentOps.harvest();
	}
	
	public void market() {
		currentOps.market();
	}
	
	public void setSeason(Season season) {
		currentOps = map.get(season);
	}
}
