package assignments.farmerFinal.core;

import assignments.farmerFinal.amishOps.AmishFall;
import assignments.farmerFinal.amishOps.AmishSpring;
import assignments.farmerFinal.amishOps.AmishSummer;
import assignments.farmerFinal.conventionalOps.ConventionalFall;
import assignments.farmerFinal.conventionalOps.ConventionalSpring;
import assignments.farmerFinal.conventionalOps.ConventionalSummer;
import assignments.farmerFinal.core.Farmer.Season;
import assignments.farmerFinal.organicOps.OrganicFall;
import assignments.farmerFinal.organicOps.OrganicSpring;
import assignments.farmerFinal.organicOps.OrganicSummer;

public class FarmerMain {
	public static void main (String [] args) {
		Season[] seasons = {Season.SPRING, Season.SUMMER, Season.FALL};
		Farmer conventionalFarmer = new Farmer(new ConventionalSpring(),new ConventionalSummer(),new ConventionalFall());
		Farmer organicFarmer = new Farmer(new OrganicSpring(),new OrganicSummer(),new OrganicFall());
		Farmer amishFarmer = new Farmer(new AmishSpring(),new AmishSummer(),new AmishFall());
		Farmer[] farmers = {conventionalFarmer,organicFarmer,amishFarmer};
		for (Season season : seasons) {
			for (Farmer farmer : farmers) {
				System.out.println(season);
				farmer.setSeason(season);
				farmer.plow();
				farmer.plant();
				farmer.harvest();
				farmer.weedControl();
				farmer.market();
			}
		}
	}
}
