package assignments.turnStyle;

public class LockedState implements State{

	TurnStyle turnStyle;
	public LockedState(TurnStyle turnStyle) {
		this.turnStyle = turnStyle;
	}
	
	@Override
	public String handleCoinInsertion() {
		turnStyle.setState(turnStyle.openState);
		return "Thanks";
	}

	@Override
	public String handleEntry() {
		return "Halt!";
	}

	@Override
	public String handleLeave() {
		return "Sound the alarm!";
	}

}
