package assignments.turnStyle;

public interface State {
	public String handleCoinInsertion();
	public String handleEntry();
	public String handleLeave();
}
