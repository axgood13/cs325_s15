package assignments.turnStyle;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import frs.alphamon.AlphamonFactory;
import frs.core.GameImpl;

public class TurnStyleTest {
	TurnStyle turnStyle;
	
	@Before
	public void setup() {
		turnStyle = new TurnStyle();
	}
	
	@Test
	public void shouldBeLockedToStartAndWarning() {
		assertTrue(turnStyle.getState() == turnStyle.lockedState);
		assertFalse(turnStyle.enter().equals("Proceed with caution."));
	}
	
	@Test
	public void shouldWarnIfTryToEnterThroughLocked() {
		assertTrue(turnStyle.getState() == turnStyle.lockedState);
		assertTrue(turnStyle.enter().equals("Halt!"));
	}
	
	@Test
	public void shouldSoundAlarmIfSomeoneGoesThroughLocked() {
		assertTrue(turnStyle.getState() == turnStyle.lockedState);
		assertFalse(turnStyle.enter().equals("Sound the alarm!"));
		assertTrue(turnStyle.leave().equals("Sound the alarm!"));
	}
	
	@Test
	public void switchesStateAfterCoinEntered() {
		assertTrue(turnStyle.getState() == turnStyle.lockedState);
		assertTrue(turnStyle.insertCoin().equals("Thanks"));
		assertTrue(turnStyle.getState() == turnStyle.openState);
	}
	
	@Test
	public void openStateAllowsEntry() {
		turnStyle.insertCoin();
		assertTrue(turnStyle.getState() == turnStyle.openState);
		assertTrue(turnStyle.enter().equals("Proceed with caution."));
	}
	
	@Test
	public void openStateRejectsMultipleCoins() {
		turnStyle.insertCoin();
		assertTrue(turnStyle.getState() == turnStyle.openState);
	}

}
