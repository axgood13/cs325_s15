package assignments.turnStyle;

public class TurnStyle {
	State state;
	public OpenState openState = new OpenState(this);
	public LockedState lockedState = new LockedState(this);
	
	public TurnStyle() {
		setState(lockedState);
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public State getState() {
		return state;
	}
	
	public String insertCoin() {
		return state.handleCoinInsertion();
	}
	
	public String enter() {
		return state.handleEntry();
	}
	
	public String leave() {
		return state.handleLeave();
	}
}
