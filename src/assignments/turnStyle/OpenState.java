package assignments.turnStyle;

public class OpenState implements State{
	TurnStyle turnStyle;
	
	public OpenState(TurnStyle turnStyle) {
		this.turnStyle = turnStyle;
	}
	
	@Override
	public String handleCoinInsertion() {
		return "Coins rejected.";
	}

	@Override
	public String handleEntry() {
		return "Proceed with caution.";
	}

	@Override
	public String handleLeave() {
		return "Have a nice day.";
	}

}
