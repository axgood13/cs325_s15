package frs.core;

public interface TurnChanger {
	int changeTurns();
	int signOfStartingPlayer();
	void setLastDiceThrown(int[] lastDiceThrown);
}


