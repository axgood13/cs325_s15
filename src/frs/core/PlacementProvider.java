package frs.core;

import frs.core.Board.Placement;

public interface PlacementProvider {
	Placement[] placementArray();
}
