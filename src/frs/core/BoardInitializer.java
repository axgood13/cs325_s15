package frs.core;

import frs.core.Board.Placement;

public interface BoardInitializer {
	public void initBoard(Placement[] placementArray, Board board) throws Exception;
	public void initBoard(Board board) throws Exception;
	public int getRedCount();
	public int getBlackCount();
}
