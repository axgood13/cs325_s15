package frs.core;

public interface DoublesHandler {
	public int[] handleDoublesAndReturnAllDice(int[] diceThrown);
}
