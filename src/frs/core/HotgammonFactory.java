package frs.core;

import javax.swing.JTextField;

import frs.core.Board.Placement;
import frs.hotgammon.Game;
import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.DrawingView;
import frs.minidraw.framework.Factory;

public interface HotgammonFactory extends Factory{
	public MoveRules createMoveRules(Game game);
	public WinnerDeterminer createWinnerDeterminer(Game game);
	public TurnChanger createTurnChanger(Game game);
	public DiceRoller createDiceRoller();
	public PlacementProvider createPlacementProvider();
	public DoublesHandler createDoublesHandler();
	public DrawingView createDrawingView(DrawingEditor editor);
	public void setPlacementArray(Placement[] placementArray);
	public Drawing createDrawing(DrawingEditor editor);
	public JTextField createStatusField(DrawingEditor editor);
}
