package frs.core;

import java.util.ArrayList;

import frs.core.Board.Placement;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.GameObserver;
import frs.hotgammon.Location;

/**
 * Skeleton implementation of HotGammon.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Department of Computer Science Aarhus University
 * 
 * Please visit http://www.baerbak.com/ for further information.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class GameImpl implements Game {
	private Board board = new Board();
	private int[] diceValuesLeft;
	private int[] diceThrown;
	private int[] lastDiceThrown = null;
	private ArrayList<Integer> movesMade = new ArrayList<Integer>();
	private GameObserver observer;
	private int playerInTurnSign = 0;
	private int throwCount = 0;

	private DiceRoller diceRoller;
	private TurnChanger turnChanger;
	private DoublesHandler doublesHandler;
	private WinnerDeterminer winnerDeterminer;
	private MoveRules moveRules;
	private PlacementProvider placementProvider;

	public GameImpl(HotgammonFactory factory) {
		this.diceRoller = factory.createDiceRoller();
		this.turnChanger = factory.createTurnChanger(this);
		this.doublesHandler = factory.createDoublesHandler();
		this.winnerDeterminer = factory.createWinnerDeterminer(this);
		this.moveRules = factory.createMoveRules(this);
		this.placementProvider = factory.createPlacementProvider();
	}

	public static class Point {
		Color player = Color.NONE;
		int count = 0;

		public Point() {
		}

		public Point(Color player, int count) {
			this.player = player;
			this.count = count;
		}

		public Color getPlayer() {
			return player;
		}

		public void setPlayer(Color color) {
			this.player = color;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}
	}

	public void initBlankBoard() {
		for (int i = 0; i < Location.values().length; i++) {
			board.addPlacement(new Placement(Location.values()[i], new Point()));
		}
	}

	private void configureBoardWithArray() {
		Placement[] placementArray = placementProvider.placementArray();
		for (Placement placement : placementArray) {
			board.addPlacement(placement);
		}
	}

	public void newGame() {
		initBlankBoard();
		if (placementProvider.placementArray() != null) {
			configureBoardWithArray();
		}
	}

	public void nextTurn() {
		setDiceThrown(diceRoller);
		if (throwCount == 0) {
			while (diceThrown[1] == diceThrown[0]) { // Roll the dice again if dice are the same.
				setDiceThrown(diceRoller);
			}
			playerInTurnSign = turnChanger.signOfStartingPlayer();
		}
		if (throwCount > 0) {
			playerInTurnSign = turnChanger.changeTurns();
		}
		observer.diceRolled(diceThrown);
		throwCount++;
		movesMade.clear();
		lastDiceThrown = diceThrown;
		diceValuesLeft = diceThrown;
		turnChanger.setLastDiceThrown(lastDiceThrown);
	}

	public boolean move(Location from, Location to) {
		if (winner() == Color.NONE) {
			Color playerPickedUp = board.get(from).getPlayer();
			if (moveRules.isValidMove(from,to) && getPlayerInTurn() == playerPickedUp) {
				makeMoveOnBoard(from, to);
				movesMade.add(Math.abs(Location.distance(from, to)));
				diceValuesLeft = generateDiceValuesLeft();
				observer.checkerMove(from, to);
				return true;
			} else {
				observer.checkerMove(to, from);
				return false;
			}
		}
		return false;
	}

	private void makeMoveOnBoard(Location from, Location to) {
		Point fromPoint = board.get(from);
		Point toPoint = board.get(to);
		if (isCapture(getPlayerInTurn(), to)) {
			Location bar;
			if (getPlayerInTurn() == Color.BLACK) {
				bar = Location.R_BAR;
			} else {
				bar = Location.B_BAR;
			}
			toPoint.setCount(toPoint.getCount() + 1);
			makeMoveOnBoard(to,bar);
		} else {
			toPoint.setCount(toPoint.getCount() + 1);
		}
		if (to == Location.R_BAR || to == Location.B_BAR) {
			toPoint.setPlayer(Color.getColorFromNumerical(-playerInTurnSign));
		}
		else {
			toPoint.setPlayer(getPlayerInTurn());
		}
		fromPoint.setCount(fromPoint.getCount() - 1);
		if (fromPoint.getCount() == 0) {
			fromPoint.setPlayer(Color.NONE);
		}
	}

	private int[] generateDiceValuesLeft() {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		int[] newDiceValues = new int[diceValuesLeft.length - 1];
		try {
			if (diceThrown.length == 4) {
				for (int i = 0; i < newDiceValues.length; i++) {
					newDiceValues[i] = diceThrown[0];
				}
			} else {
				for (int move : movesMade) {
					for (int die : diceThrown) {
						if (move != die) {
							arrayList.add(die);
						}
					}
				}
				if (arrayList.size() > 0) {
					for (int i = 0; i < newDiceValues.length; i++) {
						newDiceValues[i] = arrayList.get(i);
					}
				}
			}
		} catch (Exception e) {
		}
		return newDiceValues;
	}

	public Color getPlayerInTurn() {
		return Color.getColorFromNumerical(playerInTurnSign);
	}

	public int getNumberOfMovesLeft() {
		return diceValuesLeft.length;
	}

	public void setDiceThrown(DiceRoller diceRoller) {
		diceThrown = diceRoller.rollDice(throwCount);
		diceThrown = doublesHandler.handleDoublesAndReturnAllDice(diceThrown);
	}

	public int[] diceThrown() {
		return diceThrown;
	}

	public int[] diceValuesLeft() { // Sorted from largest die to smallest
		if (diceValuesLeft.length > 1) {
			if (diceValuesLeft[0] < diceValuesLeft[1]) {
				int temp = diceValuesLeft[0];
				diceValuesLeft[0] = diceValuesLeft[1];
				diceValuesLeft[1] = temp;
			}
		}
		return diceValuesLeft;
	}

	public Color winner() {
		winnerDeterminer.setThrowCount(throwCount);
		try {
			return winnerDeterminer.winner();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Color getColor(Location location) {
		return board.get(location).player;
	}

	public int getCount(Location location) {
		return board.get(location).count;
	}
	
	private boolean isCapture(Color playerInTurn, Location to) {
		return playerInTurn != getColor(to) && getCount(to) == 1;
	}

	@Override
	public void addGameObserver(GameObserver observer) {
		this.observer = observer;
	}
}
