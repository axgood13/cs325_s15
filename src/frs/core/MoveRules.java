package frs.core;

import frs.hotgammon.Location;

public interface MoveRules {
	/**
	 * Method for determining if a move from a location to another is valid or
	 * not.
	 * 
	 * @param from
	 * @param to
	 * @return whether or not the move is valid.
	 */
	public boolean isValidMove(Location from, Location to);
}
