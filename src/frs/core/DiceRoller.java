package frs.core;

public interface DiceRoller {
	int[] rollDice(int throwCount);
}
