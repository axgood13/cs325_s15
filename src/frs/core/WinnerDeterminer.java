package frs.core;

import frs.hotgammon.Color;

public interface WinnerDeterminer {
	Color winner() throws Exception;

	void setThrowCount(int throwCount);
}
