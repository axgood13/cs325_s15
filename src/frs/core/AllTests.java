package frs.core;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import frs.alphamon.AlphamonTest;
import frs.backgammon.BackgammonTest;
import frs.betamon.BetamonTest;
import frs.deltamon.DeltamonTest;
import frs.epsilonmon.EpsilonmonTest;
import frs.gammamon.GammamonTest;
import frs.hotgammon.LocationTest;
import frs.semimon.SemimonTest;
import frs.zetamon.ZetamonTest;

@RunWith(Suite.class)
@SuiteClasses({
	BetamonTest.class, AlphamonTest.class, LocationTest.class,
	GammamonTest.class, DeltamonTest.class, EpsilonmonTest.class,
	ZetamonTest.class, SemimonTest.class, BackgammonTest.class
})

public class AllTests {

}
