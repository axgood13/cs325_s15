package frs.core;

import java.util.HashMap;

import frs.core.GameImpl.Point;
import frs.hotgammon.Location;

public class Board {
	public static HashMap<Location, Point> board;
	public Board() {
		board = new HashMap<Location, Point>();
	}
	
	public static class Placement {
		public Location location;
		public Point point;
		
		public Placement(Location location, Point point) {
			this.location = location;
			this.point = point;
		}
	}
	
	public void addPlacement(Placement placement) {
		board.put(placement.location, placement.point);
	}
	
	public void addPlacement(Location location, Point point) {
		board.put(location, point);
	}
	
	public Point removePlacement(Location location) {
		return board.remove(location);
	}
	
	public void clearAll() {
		for (Location location : Location.values()) {
			board.remove(location);
			board.put(location, new Point());
		}
	}
	
	public Point get(Location location) {
		return board.get(location);
	}
	
	
}
