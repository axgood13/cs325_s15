package frs.betamon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.GameImpl.Point;
import frs.core.HotgammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

/**
 * Testing skeleton for AlphaMon.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Department of Computer Science Aarhus University
 * 
 * Please visit http://www.baerbak.com/ for further information.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class BetamonTest {
	private Game game;
	private HotgammonFactory factory;

	@Before
	public void setup() {
		factory = new BetamonFactory();
		game = new GameImpl(factory);
	}

	@Test
	public void shouldHaveNoPlayerInTurnAfterNewGame() {
		game.newGame();
		assertEquals(Color.NONE, game.getPlayerInTurn());
	}

	@Test
	public void shouldHaveBlackPlayerInTurnAfterNewGame() {
		game.newGame();
		game.nextTurn(); // will throw [1,2] and thus black starts
		assertEquals(Color.BLACK, game.getPlayerInTurn());
	}

	@Test
	public void shouldRoll12onFirstRoll() {
		game.newGame();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertEquals(1, roll[0]);
		assertEquals(2, roll[1]);
	}

	@Test
	public void shouldRoll34onSecondRoll() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertEquals(3, roll[0]);
		assertEquals(4, roll[1]);
	}

	@Test
	public void shouldRoll56onThirdRoll() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertEquals(5, roll[0]);
		assertEquals(6, roll[1]);
	}

	@Test
	public void shouldRoll12on1Mod3() {
		game.newGame();
		for (int i = 0; i < 10; i++) {
			game.nextTurn();
		}
		int[] roll = game.diceThrown();
		assertEquals(1, roll[0]);
		assertEquals(2, roll[1]);
	}

	@Test
	public void shouldHaveTwoBlackCheckersOnR1() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		assertEquals(2, game.getCount(Location.R1));
	}

	@Test
	public void blackCanMoveR1R2() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R1, Location.R2));
		assertEquals(1, game.getCount(Location.R1));
		assertEquals(1, game.getCount(Location.R2));
	}

	@Test
	public void blackCannotMoveR1B1() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertFalse(game.move(Location.R1, Location.B1));
	}

	@Test
	public void afterTwoBlackMovesNoMovesLeft() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R1, Location.R2));
		assertTrue(game.move(Location.R1, Location.R3));

		assertEquals(0, game.getNumberOfMovesLeft());
	}

	@Test
	public void redInTurnAfter2ndNextTurn() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		assertEquals(Color.RED, game.getPlayerInTurn());

		int[] roll = game.diceThrown();
		assertEquals(3, roll[0]);
		assertEquals(4, roll[1]);
	}

	@Test
	public void oneMoveLeftAfterOneMove() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		game.move(Location.R1, Location.R2);
		assertEquals(1, game.getNumberOfMovesLeft());
	}

	@Test
	public void gameShouldEndAfterSixRolls() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertTrue(game.winner() != null && game.winner() != Color.NONE);
	}

	@Test
	public void noWinnerUntilGameIsOver() {
		game.newGame();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertTrue(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
	}

	@Test
	public void winnerIsAlwaysRed() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertFalse(game.winner() == Color.BLACK);
		assertFalse(game.winner() == Color.NONE);
		assertTrue(game.winner() == Color.RED);
	}

	@Test
	public void canMoveToAnyOpenLocation() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2)) ,
									new Placement(Location.R6, new Point(Color.BLACK, 2)) ,
									new Placement(Location.R8, new Point(Color.RED, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R1, Location.R2));
		assertFalse(game.move(Location.R6, Location.R8));
	}

	@Test
	public void cantMoveWhenNotInTurn() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2)) ,
									new Placement(Location.B1, new Point(Color.RED, 2)) };
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertFalse(game.move(Location.B1, Location.B2));
	}

	@Test
	public void playerCanOnlyMoveInTheirSpecificDirection() {
		Placement[] placementArray = {new Placement(Location.R6, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertFalse(game.move(Location.R6, Location.R5));
	}

	@Test
	public void distanceTravelledIsValueOfRolledDie() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R1, Location.R2));
		assertFalse(game.move(Location.R1, Location.R2));
	}

	@Test
	public void capturedOpponentMovesToBar() {
		Placement[] placementArray = {new Placement(Location.R6, new Point(Color.BLACK, 2)) ,
									new Placement(Location.R7, new Point(Color.RED, 1))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R6, Location.R7));
		assertEquals(1, game.getCount(Location.R7));
		assertEquals(1, game.getCount(Location.R6));
		assertEquals(Color.BLACK, game.getColor(Location.R7));
		assertEquals(Color.BLACK, game.getColor(Location.R6));
		assertEquals(1, game.getCount(Location.R_BAR));
	}

	@Test
	public void checkerInBarMustMoveToOpponentInnerTableAndMustMoveFirst() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2)) , 
									new Placement(Location.R6, new Point(Color.BLACK, 2)) , 
									new Placement(Location.R7, new Point(Color.RED, 1)) , 
									new Placement(Location.B1, new Point(Color.RED, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R6, Location.R7));
		assertTrue(game.move(Location.R1, Location.R3));
		game.nextTurn();
		assertEquals(Color.RED, game.getPlayerInTurn());
		assertFalse(game.move(Location.B1, Location.B4));
		assertTrue(game.move(Location.R_BAR, Location.B3));
		assertFalse(game.move(Location.R_BAR, Location.B4));
	}
	 @Test
	 public void cantBearOffUnlessAllPiecesAreHome() { 
		 Placement[] placementArray = {new Placement(Location.B5, new Point(Color.BLACK, 2)) , 
				 					new Placement(Location.B7, new Point(Color.BLACK, 2))};
		 factory.setPlacementArray(placementArray);	
		 game.newGame();
		 game.nextTurn();
		 assertTrue(game.move(Location.B7, Location.B5));
		 assertTrue(game.move(Location.B7, Location.B6));
		 game.nextTurn();
		 game.nextTurn();
		 assertTrue(game.move(Location.B5, Location.B_BEAR_OFF));
		 assertTrue(game.move(Location.B6, Location.B_BEAR_OFF));
	 }
	 @Test
	 public void bearingAllPiecesOffDoesNothing() {
		 Placement[] placementArray = {
				 new Placement(Location.B1, new Point(Color.BLACK, 2)),
				 new Placement(Location.R8, new Point(Color.RED, 2))
					};
		 factory.setPlacementArray(placementArray);	
		 game.newGame();
		 game.nextTurn();
		 assertTrue(game.move(Location.B1, Location.B_BEAR_OFF));
		 assertTrue(game.move(Location.B1, Location.B_BEAR_OFF));
		 assertEquals(Color.NONE, game.winner());
	 }
	 @Test 
	 public void noWinnerEverAfter6Rolls() {
		 game.newGame();
		 game.nextTurn();
		 for (int i = 0; i < 10; i++) {
			 game.nextTurn();
		 }
		 assertEquals(Color.NONE, game.winner());
	 }
	 
	 @Test 
	 public void cantBearOffLowBeforeHighIfNotExactMatch() {
		 Placement[] placementArray = {
				 new Placement(Location.B1, new Point(Color.BLACK, 1)),
				 new Placement(Location.B4, new Point(Color.BLACK, 1))
					};
		 factory.setPlacementArray(placementArray);
		 game.newGame();
		 game.nextTurn();
		 game.nextTurn();
		 game.nextTurn();
		 assertFalse(game.move(Location.B1, Location.B_BEAR_OFF));
		 assertTrue(game.move(Location.B4, Location.B_BEAR_OFF));
		 assertTrue(game.move(Location.B1, Location.B_BEAR_OFF));
	 }
	 
	 @Test
	 public void canMoveLowerToBearOffIfExactMatch() {
		 Placement[] placementArray = {
				 new Placement(Location.B1, new Point(Color.BLACK, 1)),
				 new Placement(Location.B4, new Point(Color.BLACK, 1))
					};
		 factory.setPlacementArray(placementArray);
		 game.newGame();
		 game.nextTurn();
		 assertTrue(game.move(Location.B1, Location.B_BEAR_OFF));
		 assertTrue(game.move(Location.B4, Location.B2));
	 }
		  
	@Test
	public void canBearOffLowerCheckerIfThereAreNoHigherCheckers() {
		Placement[] placementArray = {
				new Placement(Location.R2, new Point(Color.RED, 1)), new Placement(Location.R5, new Point(Color.RED, 1))
				};
		factory.setPlacementArray(placementArray);
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		assertEquals(false, game.move(Location.R2, Location.R_BEAR_OFF));
		assertEquals(true, game.move(Location.R5, Location.R1));
		assertEquals(true, game.move(Location.R2, Location.R_BEAR_OFF)); 
	}
}
