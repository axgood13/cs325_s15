package frs.hotgammon.view;

import java.awt.Point;

import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.GameObserver;
import frs.hotgammon.Location;
import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.Figure;
import frs.minidraw.standard.ImmovableImageFigure;
import frs.minidraw.standard.MovableImageFigure;
import frs.minidraw.standard.StandardDrawing;

public class HotgammonDrawing extends StandardDrawing
								implements GameObserver{

	Game game;
	DrawingEditor editor;
	public HotgammonDrawing(Game game, DrawingEditor editor) {
		this.game = game;
		this.editor = editor;
	}
	@Override
	public void checkerMove(Location from, Location to) {
		unlock();
		MovableImageFigure movedFigure;
		MovableImageFigure capturedFigure;
		try {
			if (editor.drawing().selection().size() == 2) { 
				capturedFigure = (MovableImageFigure) editor.drawing().selection().get(1);
				movedFigure = (MovableImageFigure) editor.drawing().selection().get(0);
				if (game.getPlayerInTurn() == Color.RED) {
					capturedFigure.moveBy(Convert.locationAndCount2xy(Location.B_BAR, game.getCount(Location.B_BAR) - 1).x - capturedFigure.displayBox().x, 
							Convert.locationAndCount2xy(Location.B_BAR, game.getCount(Location.B_BAR) - 1).y - capturedFigure.displayBox().y);
				}
				else {
					capturedFigure.moveBy(Convert.locationAndCount2xy(Location.R_BAR, game.getCount(Location.R_BAR) - 1).x - capturedFigure.displayBox().x, 
							Convert.locationAndCount2xy(Location.R_BAR, game.getCount(Location.R_BAR) - 1).y - capturedFigure.displayBox().y);
				}
			}
			else {
				movedFigure = (MovableImageFigure) editor.drawing().selection().get(0);
			}
			movedFigure.moveBy(Convert.locationAndCount2xy(to, game.getCount(to) - 1).x - movedFigure.displayBox().x,
					Convert.locationAndCount2xy(to, game.getCount(to) - 1).y - movedFigure.displayBox().y);
		} catch(Exception e) {	
		}
		lock();
		editor.drawing().selection().clear();
	}

	@Override
	public void diceRolled(int[] values) {
		Drawing model = editor.drawing();
		model.unlock();
		if (editor.drawing().findFigure(166, 202) != null) {
			model.remove(model.findFigure(166,202));
			model.remove(model.findFigure(356,202));
		}
		model.remove(model.findFigure(306, 202));
		model.remove(model.findFigure(216, 202));
		model.add(new ImmovableImageFigure("die" + Integer.toString(values[1]), new Point(306, 202)));
		model.add(new ImmovableImageFigure("die" + Integer.toString(values[0]), new Point(216, 202)));
		
		if (game.diceThrown().length == 4) {
			model.add(new ImmovableImageFigure("die" + Integer.toString(values[2]), new Point(166, 202)));
			model.add(new ImmovableImageFigure("die" + Integer.toString(values[3]), new Point(356, 202)));
		}
		model.lock();
	}
	
	public boolean isCapture(Location to) {
		Figure toFigure = editor.drawing().findFigure(Convert.locationAndCount2xy(to, game.getCount(to)).x,Convert.locationAndCount2xy(to, game.getCount(to)).y);
		return toFigure != null && game.getColor(to) != game.getPlayerInTurn();
	}

}
