package frs.hotgammon.view;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.Tool;
import frs.minidraw.standard.ImageFigure;
import frs.minidraw.standard.ImmovableImageFigure;
import frs.minidraw.standard.MovableImageFigure;
import frs.minidraw.standard.SelectionTool;

public class HotgammonTool extends SelectionTool {
	private DrawingEditor editor;
	private Game game;

	public HotgammonTool(DrawingEditor editor, Game game) {
		super(editor);
		this.game = game;
		this.editor = editor;
	}

	public MoveTool moveTool() {
		return new MoveTool(editor, game);
	}

	public DieRollTool dieRollerTool() {
		return new DieRollTool(editor, game);
	}

	class MoveTool extends HotgammonTool {
		Game game;
		DrawingEditor editor;
		private int startX, startY, currentX, currentY;

		public MoveTool(DrawingEditor editor, Game game) {
			super(editor, game);
			this.game = game;
			this.editor = editor;
		}

		@Override
		public void mouseDown(MouseEvent e, int x, int y) {
			Drawing model = editor().drawing();
			MovableImageFigure figureAtTopOfStack = (MovableImageFigure) model
					.findFigure(
							(Convert.locationAndCount2xy(
									Convert.xy2Location(x, y),
									game.getCount(Convert.xy2Location(x, y)) - 1).x),
							(Convert.locationAndCount2xy(
									Convert.xy2Location(x, y),
									game.getCount(Convert.xy2Location(x, y)) - 1).y));
			model.lock();
			try {
				draggedFigure = model.findFigure(e.getX(), e.getY());
			} catch (Exception e1) {
			}
			if (draggedFigure instanceof MovableImageFigure) {
				if (figureAtTopOfStack != draggedFigure) {
					moveFigureAtTopOfStackToDraggedFiguresSpot(figureAtTopOfStack);
				}
				startX = x;
				startY = y;
				if (draggedFigure != null
						&& draggedFigure instanceof MovableImageFigure) {
					editor.showStatus("Checker selected");
					fChild = createDragTracker(draggedFigure);
				} else {
					if (!e.isShiftDown()) {
						model.clearSelection();
					}
					fChild = createAreaTracker();
				}
				fChild.mouseDown(e, x, y);
			}
		}

		@Override
		public void mouseDrag(MouseEvent e, int x, int y) {
			if (draggedFigure != null
					&& draggedFigure instanceof MovableImageFigure) {
				fChild.mouseDrag(e, x, y);
				currentX = x;
				currentY = y;
			}
		}

		@Override
		public void mouseUp(MouseEvent e, int x, int y) {
			if (draggedFigure instanceof MovableImageFigure) {
				currentX = x;
				currentY = y;
				Location currentLoc = Convert.xy2Location(currentX, currentY);
				if (isCapture(currentLoc)
						&& moveIsInDiceRolled()
						&& (game.getCount(barOfCurrentPlayer()) == 0 || Convert
								.xy2Location(startX, startY) == barOfCurrentPlayer())
						&& game.getColor(currentLoc) == game.getPlayerInTurn()) {
					Point capturedPoint = Convert.locationAndCount2xy(
							currentLoc, game.getCount(currentLoc) - 1);
					editor.drawing().addToSelection(
							editor.drawing().findFigure(capturedPoint.x,
									capturedPoint.y));
				}
				if (game.move(Convert.xy2Location(startX, startY),
						Convert.xy2Location(currentX, currentY))) {
					if (game.winner() == null
							|| game.winner() == Color.NONE) {
						if (playerCanMove()) { 
							editor.showStatus("It's "
									+ game.getPlayerInTurn()
									+ "'s turn! Dice left: "
									+ diceToString(game.diceValuesLeft()));
						} 
						else {
							if (game.diceValuesLeft().length > 0) {
								editor.showStatus("No possible moves left. Please click a die to forfeit your turn.");
							}
							else {
								editor.showStatus(game.getPlayerInTurn()
										+ "'s turn is over. Please click a die.");
							} 
							editor.setTool(dieRollerTool());
						}
					}
					else {
						editor.setTool(winnerTool());
					}
				} 
				else {
					editor.showStatus("Invalid move, try again. It's "
										+ game.getPlayerInTurn()
										+ "'s turn! Dice left: "
										+ diceToString(game.diceValuesLeft()));
				}
			}
		}

		private Tool winnerTool() {
			return new WinnerTool(editor, game);
		}

		private Location barOfCurrentPlayer() {
			if (game.getPlayerInTurn() == Color.RED) {
				return Location.R_BAR;
			} else {
				return Location.B_BAR;
			}
		}

		private boolean moveIsInDiceRolled() {
			int[] diceValuesLeft = game.diceValuesLeft();
			int distance = Math.abs(Location.distance(
					Convert.xy2Location(startX, startY),
					Convert.xy2Location(currentX, currentY)));
			for (int die : diceValuesLeft) {
				if (distance == die) {
					return true;
				}
			}
			return false;
		}

		private boolean isCapture(Location currentLocation) {
			return game.getPlayerInTurn() != game.getColor(currentLocation)
					&& game.getCount(currentLocation) == 1;
		}

		private void moveFigureAtTopOfStackToDraggedFiguresSpot(
				MovableImageFigure figureAtTopOfStack) {
			Rectangle topFigureBox = figureAtTopOfStack.displayBox();
			Rectangle draggedFigureBox = draggedFigure.displayBox();
			figureAtTopOfStack.moveBy(draggedFigureBox.x - topFigureBox.x,
					draggedFigureBox.y - topFigureBox.y);
		}
	};

	class DieRollTool extends HotgammonTool {
		Game game;
		DrawingEditor editor;
		ImageFigure clickedFigure;

		public DieRollTool(DrawingEditor editor, Game game) {
			super(editor, game);
			this.game = game;
			this.editor = editor;
		}

		@Override
		public void mouseDown(MouseEvent e, int x, int y) {
			Drawing model = editor().drawing();
			model.lock();
			clickedFigure = (ImageFigure) model.findFigure(e.getX(), e.getY());
		}

		@Override
		public void mouseUp(MouseEvent e, int x, int y) {
			if (clickedFigure instanceof ImmovableImageFigure) {
				game.nextTurn();
				if (playerIsInBar() && !canMoveOffBar()) {
					editor.showStatus("No possible moves left. Please click a die to forfeit your turn.");
					editor.setTool(dieRollerTool());
				} else {
					editor.showStatus("It's " + game.getPlayerInTurn()
							+ "'s turn! Dice left: "
							+ diceToString(game.diceValuesLeft()));
					editor.setTool(moveTool());
				}
			}
		}
	};

	private boolean canMoveOffBar() {
		Location[] redHomeLocations = { Location.R_BEAR_OFF, Location.R1,
				Location.R2, Location.R3, Location.R4, Location.R5, Location.R6 };
		Location[] blackHomeLocations = { Location.B_BEAR_OFF, Location.B1,
				Location.B2, Location.B3, Location.B4, Location.B5, Location.B6 };
		Location possibleLocation;
		if (game.getPlayerInTurn() == Color.RED) {
			for (int die : game.diceValuesLeft()) {
				possibleLocation = blackHomeLocations[die];
				if ((game.getColor(possibleLocation) == Color.RED || game
						.getColor(possibleLocation) == Color.NONE)
						|| game.getCount(possibleLocation) == 1) {
					return true;
				}
			}
		} else {
			for (int die : game.diceValuesLeft()) {
				possibleLocation = redHomeLocations[die];
				if ((game.getColor(possibleLocation) == Color.BLACK || game
						.getColor(possibleLocation) == Color.NONE)
						|| game.getCount(possibleLocation) == 1) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean playerIsInBar() {
		if (game.getPlayerInTurn() == Color.RED) {
			return game.getCount(Location.R_BAR) > 0;
		} else {
			return game.getCount(Location.B_BAR) > 0;
		}
	}

	private String diceToString(int[] dieArray) {
		String string = "";
		for (int i = 0; i < dieArray.length; i++) {
			if (i == dieArray.length - 1) {
				string += Integer.toString(dieArray[i]);
			} else {
				string += Integer.toString(dieArray[i]) + ", ";
			}
		}
		return string;
	}
	
	private boolean playerCanMove() {
		if (playerIsInBar() && !canMoveOffBar()) {
			return false;
		}
		for (Location location : populatedLocationsOfPlayerInTurn()) {
			for (int die : game.diceValuesLeft()) {	
				if (toLocationIsCorrectColorOrOneOpponent(Location.findLocation(game.getPlayerInTurn(), location, die))) {
					if (playerIsBearingOff(location,die)) { 
						if (playerHasAllCheckersHome() &&
								toLocationIsLegalBearOff(location, Location.findLocation(game.getPlayerInTurn(), location, die))) {
							return true;
						}
					}
					else {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private boolean playerHasAllCheckersHome() {
		Location[] redHomeLocations = {Location.R1, Location.R2, Location.R3, Location.R4, Location.R5, Location.R6, Location.R_BEAR_OFF};
		Location[] blackHomeLocations = {Location.B1, Location.B2, Location.B3, Location.B4, Location.B5, Location.B6, Location.B_BEAR_OFF};
		int count = 0;
		if (game.getPlayerInTurn() == Color.RED) {
			for (Location location : redHomeLocations) {
				count += game.getCount(location);
			}
			return count == getRedCount();
		}
		else {
			for (Location location : blackHomeLocations) {
				count += game.getCount(location);
			}
			return count == getBlackCount();
		}
	}

	private boolean toLocationIsLegalBearOff(Location from, Location to) {
		if (game.getPlayerInTurn() == Color.RED) {
			Location[] redHomeLocations = {Location.R6, Location.R5, Location.R4, Location.R3, Location.R2, Location.R1, Location.R_BEAR_OFF};
			Location redHighestLocation = null;
			for (Location location : redHomeLocations) {
				if (game.getCount(location) > 0) {
					redHighestLocation = location;
					break;
				}
			}
			if (from != redHighestLocation) {
				for (int die : game.diceValuesLeft()) {
					if (Math.abs(Location.distance(from, to)) == die) {
						return true;
					}
				}
				return false;
			}
		}
		else {
			Location[] blackHomeLocations = {Location.B6, Location.B5, Location.B4, Location.B3, Location.B2, Location.B1, Location.B_BEAR_OFF};
			Location blackHighestLocation = null;
			for (Location location : blackHomeLocations) {
				if (game.getCount(location) > 0) {
					blackHighestLocation = location;
					break;
				}
			}
			if (from != blackHighestLocation) {
				for (int die : game.diceValuesLeft()) {
					if (Math.abs(Location.distance(from, to)) == die) {
						return true;
					}
				}
				return false;
			}
		}
		return true;
	}

	private boolean toLocationIsCorrectColorOrOneOpponent(Location toLocation) {
		return game.getColor(toLocation) == game.getPlayerInTurn() || 
				game.getCount(toLocation) <= 1;
	}

	private Location[] populatedLocationsOfPlayerInTurn() {
		ArrayList<Location> arrayList = new ArrayList<Location>();
		Location[] populatedPlayerLocations;
		for (Location location : Location.values()) {
			if (game.getPlayerInTurn() == game.getColor(location)) {
				arrayList.add(location);
			}
		}
		
		populatedPlayerLocations = new Location[arrayList.size()];
		for (int i = 0; i < arrayList.size(); i++) {
			populatedPlayerLocations[i] = arrayList.get(i);
		}
		return populatedPlayerLocations;
	}
	
	private int getBlackCount() {
		int blackCount = 0;
		for (Location location : Location.values()) {
			blackCount += game.getColor(location) == Color.BLACK ? game.getCount(location) : 0;
		}
		return blackCount;
	}
	
	private int getRedCount() {
		int redCount = 0;
		for (Location location : Location.values()) {
			redCount += game.getColor(location) == Color.RED ? game.getCount(location) : 0;
		}
		return redCount;
	}
	
	private boolean playerIsBearingOff(Location location, int die) {
		return Location.findLocation(game.getPlayerInTurn(), location, die) == Location.R_BEAR_OFF || 
				Location.findLocation(game.getPlayerInTurn(), location, die) == Location.B_BEAR_OFF;
	}
	
	public class WinnerTool extends HotgammonTool {

		DrawingEditor editor;
		ImageFigure clickedFigure;

		public WinnerTool(DrawingEditor editor, Game game) {
			super(editor, game);
			this.editor = editor;
			editor.showStatus("The game is over. " + game.winner() + " wins!");
		}

		@Override
		public void mouseDown(MouseEvent e, int x, int y) {
			Drawing model = editor().drawing();
			model.lock();
		}
	}
}
