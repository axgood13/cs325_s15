package frs.hotgammon.visual;

import java.awt.Point;

import frs.backgammon.BackgammonFactory;
import frs.core.HotgammonFactory;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.Figure;
import frs.minidraw.standard.ImageFigure;
import frs.minidraw.standard.MiniDrawApplication;
import frs.minidraw.standard.SelectionTool;

/** Show the dice and some checkers on the
 * backgammon board.  
 * 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
public class ShowCheckersAndDice {
	HotgammonFactory factory;
	public ShowCheckersAndDice(HotgammonFactory factory) {
		this.factory = factory;
	}
  public static void main(String[] args) {
	  HotgammonFactory factory = new BackgammonFactory();
    DrawingEditor editor = 
      new MiniDrawApplication( "Show HotGammon figures...",  
                               factory);
    editor.open();

    Figure redDie = new ImageFigure("die4", new Point(216, 202));
    Figure blackDie = new ImageFigure("die2", new Point(306, 202));
    editor.drawing().add(redDie);
    editor.drawing().add(blackDie);
    
    Figure bc = new ImageFigure("blackchecker", new Point(21,21));
    editor.drawing().add(bc);
    Figure rc = new ImageFigure("redchecker", new Point(507,390));
    editor.drawing().add(rc);

    editor.setTool( new SelectionTool(editor) );

  }
}


