package frs.backgammon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.GameImpl;
import frs.core.HotgammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

/**
 * Testing skeleton for AlphaMon.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Department of Computer Science Aarhus University
 * 
 * Please visit http://www.baerbak.com/ for further information.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class BackgammonTest {
	private Game game;
	private HotgammonFactory factory;

	@Before
	public void setup() {
		factory = new BackgammonFactory();
		game = new GameImpl(factory);
	}

	@Test
	public void shouldHaveNoPlayerInTurnAfterNewGame() {
		game.newGame();
		assertEquals(Color.NONE, game.getPlayerInTurn());
	}

	@Test
	public void gameShouldNotEndAfterSixRolls() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertFalse(game.winner() != null && game.winner() != Color.NONE);
	}
	
	@Test 
	public void noWinnerEverAfter6Rolls() {
		game.newGame();
		game.nextTurn();
		for (int i = 0; i < 10; i++) {
			game.nextTurn();
		}
		assertEquals(Color.NONE, game.winner());
	}
	
	@Test
	public void standardGammonSpotsFilled() {
		game.newGame();
		assertEquals(2, game.getCount(Location.B1));
		assertEquals(2, game.getCount(Location.R1));
		assertEquals(5, game.getCount(Location.B6));
		assertEquals(5, game.getCount(Location.R6));
		assertEquals(3, game.getCount(Location.B8));
		assertEquals(3, game.getCount(Location.R8));
		assertEquals(5, game.getCount(Location.B12));
		assertEquals(5, game.getCount(Location.R12));
	}
	
	@Test
	public void checkThatDoubleRollIsProperlyDealtWith() {
		game.newGame();
		game.nextTurn();
		while (game.diceThrown()[0] != game.diceThrown()[1]) {
			game.nextTurn();
		}
		assertEquals(4, game.diceThrown().length);
		assertTrue(game.diceThrown()[0] == game.diceThrown()[1] &&
				game.diceThrown()[1] == game.diceThrown()[2] &&
				game.diceThrown()[2] == game.diceThrown()[3]);
	}
}
