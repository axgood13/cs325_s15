package frs.zetamon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.GameImpl;
import frs.core.HotgammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

/**
 * Testing skeleton for AlphaMon.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Department of Computer Science Aarhus University
 * 
 * Please visit http://www.baerbak.com/ for further information.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class ZetamonTest {
	private Game game;
	private HotgammonFactory factory;

	@Before
	public void setup() {
		factory = new ZetamonFactory();
		game = new GameImpl(factory);
	}

	@Test
	public void shouldHaveNoPlayerInTurnAfterNewGame() {
		game.newGame();
		assertEquals(Color.NONE, game.getPlayerInTurn());
	}

	@Test
	public void shouldHaveBlackPlayerInTurnAfterNewGame() {
		game.newGame();
		game.nextTurn(); // will throw [1,2] and thus black starts
		assertEquals(Color.BLACK, game.getPlayerInTurn());
	}

	@Test
	public void shouldRoll12onFirstRoll() {
		game.newGame();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertEquals(1, roll[0]);
		assertEquals(2, roll[1]);
	}

	@Test
	public void shouldRoll34onSecondRoll() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertEquals(3, roll[0]);
		assertEquals(4, roll[1]);
	}

	@Test
	public void shouldRoll56onThirdRoll() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertEquals(5, roll[0]);
		assertEquals(6, roll[1]);
	}

	@Test
	public void shouldRoll12on1Mod3() {
		game.newGame();
		for (int i = 0; i < 10; i++) {
			game.nextTurn();
		}
		int[] roll = game.diceThrown();
		assertEquals(1, roll[0]);
		assertEquals(2, roll[1]);
	}

	@Test
	public void shouldHaveOneBlackCheckerOnR1() {
		game.newGame();
		assertEquals(1, game.getCount(Location.R1));
	}

	@Test
	public void blackCanMoveR1R2() {
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R1, Location.R2));
		assertEquals(0, game.getCount(Location.R1));
		assertEquals(2, game.getCount(Location.R2));
	}

	@Test
	public void afterTwoBlackMovesNoMovesLeft() {
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R1, Location.R2));
		assertTrue(game.move(Location.R1, Location.R3));

		assertEquals(0, game.getNumberOfMovesLeft());
	}

	@Test
	public void redInTurnAfter2ndNextTurn() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		assertEquals(Color.RED, game.getPlayerInTurn());

		int[] roll = game.diceThrown();
		assertEquals(3, roll[0]);
		assertEquals(4, roll[1]);
	}

	@Test
	public void oneMoveLeftAfterOneMove() {
		game.newGame();
		game.nextTurn();
		game.move(Location.R1, Location.R2);
		assertEquals(1, game.getNumberOfMovesLeft());
	}

	@Test
	public void gameShouldEndAfterSixRolls() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertTrue(game.winner() != null && game.winner() != Color.NONE);
	}

	@Test
	public void noWinnerUntilGameIsOver() {
		game.newGame();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertTrue(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
	}

	@Test
	public void winnerIsAlwaysRed() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertFalse(game.winner() == Color.BLACK);
		assertFalse(game.winner() == Color.NONE);
		assertTrue(game.winner() == Color.RED);
	}

	@Test
	public void canMoveToAnyOpenLocation() {
		game.newGame();
		game.nextTurn();
		assertTrue(game.move(Location.R3, Location.R5));
		assertTrue(game.move(Location.R3, Location.R4));
	}

	@Test
	public void cantMoveWhenNotInTurn() {
		game.newGame();
		game.nextTurn();
		assertFalse(game.move(Location.B1, Location.B2));
	}
	
	@Test 
	public void noWinnerEverAfter6Rolls() {
		game.newGame();
		game.nextTurn();
		for (int i = 0; i < 10; i++) {
			game.nextTurn();
		}
		assertEquals(Color.NONE, game.winner());
	}
	
	@Test
	public void hypergammonSpotsFilledWith1() {
		game.newGame();
		Location[] array = {Location.R1, Location.R2, Location.R3,
							Location.B1, Location.B2, Location.B3};
		for (Location location : array) {
			assertEquals(1, game.getCount(location));
		}
	}
}
