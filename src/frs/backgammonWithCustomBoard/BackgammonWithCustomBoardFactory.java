package frs.backgammonWithCustomBoard;

import javax.swing.JTextField;

import frs.core.Board.Placement;
import frs.core.DiceRoller;
import frs.core.DoublesHandler;
import frs.core.HotgammonFactory;
import frs.core.MoveRules;
import frs.core.PlacementProvider;
import frs.core.TurnChanger;
import frs.core.WinnerDeterminer;
import frs.hotgammon.Game;
import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.DrawingView;
import frs.minidraw.standard.StandardDrawing;
import frs.minidraw.standard.StdViewWithBackground;
import frs.variants.dieRolls.RandomDiceRoller;
import frs.variants.doublesHandler.StandardDoublesHandler;
import frs.variants.moves.StdMoveValidator;
import frs.variants.playerInTurn.AlternatingTurnChanger;
import frs.variants.winners.BearOffFirstDeterminer;

public class BackgammonWithCustomBoardFactory implements HotgammonFactory{
	Placement[] placementArray;
	
	@Override
	public MoveRules createMoveRules(Game game) {
		return new StdMoveValidator(game);
	}

	@Override
	public WinnerDeterminer createWinnerDeterminer(Game game) {
		return new BearOffFirstDeterminer(game);
	}

	@Override
	public TurnChanger createTurnChanger(Game game) {
		return new AlternatingTurnChanger(game);
	}

	@Override
	public DiceRoller createDiceRoller() {
		return new RandomDiceRoller();
	}

	@Override
	public DoublesHandler createDoublesHandler() {
		return new StandardDoublesHandler();
	}

	@Override
	public PlacementProvider createPlacementProvider() {
		return new PlacementProvider() {
			public Placement[] placementArray() {
				return placementArray;
			}
		};
	}

	@Override
	public void setPlacementArray(Placement[] placementArray) {
		this.placementArray = placementArray;
	}

	public DrawingView createDrawingView( DrawingEditor editor ) {
	    DrawingView view = new StdViewWithBackground(editor, "board");
	    return view;
	}
	
	  public Drawing createDrawing( DrawingEditor editor ) {
		    return new StandardDrawing();
		  }

		  public JTextField createStatusField( DrawingEditor editor ) {
		    JTextField statusField = new JTextField( "Jeremy will never win" );
		    statusField.setEditable(false);
		    return statusField;
		  }
}
