package frs.minidraw.boardgame;

/** This defines the Observer pattern's observer role that allows the
 * BoardDrawing to observe a board game's game instance and react by
 * redrawing figures. A distinction is made between 'piece' which are
 * the moveable objects in a board game (like checkers), and 'props'
 * which are the objects with fixed position but changeable
 * appearance (like cards or dice).

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
public interface BoardGameObserver<LOCATION> {
  /** the update method for pieces moved from location 'from' to
   * location 'to'. 
   * @param from the LOCATION that a piece has moved from
   * @param to the LOCATION that a piece has moved to
   */
  public void pieceMovedEvent(LOCATION from, LOCATION to);

  /** the update method for props changed. 
   * @param keyOfChangedProp a string key identifying the
   * game domain object that has changed. This key must
   * be identical to that assigned in the FigureFactory's
   * 
   */
  public void propChangeEvent(String keyOfChangedProp);
}
