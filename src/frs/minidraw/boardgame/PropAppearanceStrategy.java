package frs.minidraw.boardgame;

/** This role defines a strategy (strategy pattern) encapsulating the
 * algorithms to determine the appearance of a prop used in
 * a board game.
 *
 * Create an instance of this strategy and supply it to the BoardDrawing
 * so it can find the proper image to set on a prop (BoardFigure) once
 * the underlying game emits a propChangeEvent.
 *
 * Example: In backgammon, when a die is rolled, the game instance
 * should emit a propChangeEvent for that particular die. The
 * BoardDrawing will listen/observe the propChangeEvent and
 * for the given die, request an instance of this interface to
 * generate the die image to display on the GUI. 

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
public interface PropAppearanceStrategy {
  /* return the name of an image to be used for a prop.
   * @param keyOfProp the key of the particular prop in the (key,
   * board figure) map that the FigureFactory has generated.
   * @return the string identifying the image stored in the MiniDraw
   * ImageManager that should be used for the given prop.
*/
  public String calculateImageNameForPropWithKey(String keyOfProp);
}
