package frs.minidraw.framework;

import java.awt.Rectangle;
import java.util.EventObject;

/**
 * FigureChange event passed to FigureChangeListeners.
 *

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
public class FigureChangeEvent extends EventObject {
  /**
	 * 
	 */
	private static final long serialVersionUID = 2886606380020498578L;
private Rectangle rect;
  private static final Rectangle  
    emptyRect = new Rectangle(0, 0, 0, 0);
  
  /**
   * Constructs an event for the given source Figure. The rectangle is the
   * area to be invalvidated.
   */
  public FigureChangeEvent(Figure source, Rectangle r) {
    super(source);
    rect = r;
  }
  
  /**
   * Constructs an event for the given source Figure with an empty
   * rectangle.
   */
  public FigureChangeEvent(Figure source) {
    super(source);
    rect = emptyRect;
  }
  
  /**
   *  Gets the changed figure.
   */
  public Figure getFigure() {
    return (Figure)getSource();
  }
  
  /**
   *  Gets the changed rectangle.
   */
  public Rectangle getInvalidatedRectangle() {
    return rect;
  }
}
