package frs.minidraw.framework;

import java.util.List;

/** The selection handler role defines the interface for the
    responsibility of managing a drawing's multiple figure selection mechanism.

    Responsibility
    A) Act as a collection of figures presently selected in a drawing.
    B) Support adding, removing, and state toggling figures
    C) Any adding or removing of a figure MUST be followed
    by a call to the figure's 'changed' method in order for the
    views to repaint the figure correctly! See StandardSelectionHandler.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
public interface SelectionHandler {

  /**
   * Get an iterator over all selected figures
   */
  public List<Figure> selection();
  
  /**
   * Adds a figure to the current selection.
   * @param figure the figure to add to the selection
   */
  public void addToSelection(Figure figure);
  
  /**
   * Removes a figure from the selection.
   * @param figure the figure to remove
   */
  public void removeFromSelection(Figure figure);
  
  /**
   * If a figure isn't selected it is added to the selection.
   * Otherwise it is removed from the selection.
   * @param figure the figure to toggle
   */
  public void toggleSelection(Figure figure);
  
  /**
   * Clears the current selection.
   */
  public void clearSelection();
}
