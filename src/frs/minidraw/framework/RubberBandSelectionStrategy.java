package frs.minidraw.framework;

import java.awt.Rectangle;

/** A strategy for selecting figures when using the SelectAreaTracker.
 * You can implement a non-default strategy in order to control which
 * figures are selected when rubber band selecting. One example is in
 * board games where you may select a set of checkers but this strategy
 * can remove opponent checkers so a selection operation only select your own.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

public interface RubberBandSelectionStrategy {

  /** Add figures in 'model' to the model's internal selection. The
      figures that are added to the selection is controlled by
      the 'rubberBandRectangle' (the rubber band the user has
      dragged in the view) and the toggle. PostCondition:
      model's selection is updated with the figures within
      the rubber rectangle (or what ever the strategy decides).
      @param model the drawing where figures should be selected in
      @param rubberBandRectangle the rectangle selected
      @param toggle if true then figures are not added but "toggled"
      in and out of the selection
  */
  public void selectGroup(Drawing model, 
                          Rectangle rubberBandRectangle,
                          boolean toggle);
}
