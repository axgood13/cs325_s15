package frs.minidraw.framework;

import javax.swing.JTextField;

/** Abstract factory for creating implementations of the central
    roles used in MiniDraw.

    Responsibilities:
    1) Create, upon request, instances of the roles that the 
       DrawingEditor needs.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
public interface Factory {

  /** Create the drawing view (View role of the MVC pattern). 
   * @param editor the editor that the view will be associated with.
   * @return a new drawingview
  */
  public DrawingView createDrawingView( DrawingEditor editor );

  /** Create the drawing (Model role of the MVC pattern). 
   * @param editor the editor that the view will be associated with 
   * @return a new drawing
   */
  public Drawing createDrawing( DrawingEditor editor );

  /** Create the text field used for messages.
   * @param editor the editor that the view will be associated with 
   * @return a new status field or null in case no status field is needed.
   */
  public JTextField createStatusField( DrawingEditor editor );
}
