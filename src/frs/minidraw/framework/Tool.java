package frs.minidraw.framework;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/** Tool is the Controller role in the MVC pattern for MiniDraw. A
 * tool must process all user input events and translate them into
 * modifications of the Drawing (= model role).
 *
 * The interface follows the schema defined in JHotDraw.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
public interface Tool {
  /** Handles mouse down events in the drawing view. */
  public void mouseDown(MouseEvent e, int x, int y);
  
  /** Handles mouse drag events in the drawing view (mouse button is
   * down). */
  public void mouseDrag(MouseEvent e, int x, int y);
  
  /** Handles mouse up in the drawing view. */
  public void mouseUp(MouseEvent e, int x, int y);
  
  /** Handles mouse moves (if the mouse button is up). */
  public void mouseMove(MouseEvent evt, int x, int y);
  
  /** Handles key down events in the drawing view. */
  public void keyDown(KeyEvent evt, int key);
}
