package frs.minidraw.standard;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import frs.minidraw.framework.Figure;
import frs.minidraw.framework.FigureChangeEvent;
import frs.minidraw.framework.FigureChangeListener;

/** Abstract Figure: Base implementation of some Figure behaviour.
 * Responsibilities: A) All observer role "Subject" base functionality 
 * is provided.
 *
 * Code partly copied from JHotDraw 5.1.
 *
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 *
*/

public abstract class AbstractFigure implements Figure {

  /** the listeners of this figure */
  List<FigureChangeListener> listenerList;

  /** Base construction of a figure */
  public AbstractFigure() {
    listenerList = new ArrayList<FigureChangeListener>();
  }

  public void moveBy(int dx, int dy) {
    willChange();
    basicMoveBy(dx, dy);
    changed();
  }
  
  /**
   * Informes that a figure is about to change something that
   * affects the contents of its display box.
   */
  protected void willChange() {
    invalidate();
  }

  public void invalidate() {
    for (FigureChangeListener l : listenerList ) {
      Rectangle r = (Rectangle) displayBox().clone();
      FigureChangeEvent e = new FigureChangeEvent(this, r);
      l.figureInvalidated( e );
    }
  }
  
  /**
   * This is the hook method to be overridden when a figure
   * moves. Do not call directly, instead call 'moveBy'.
   */
  protected abstract void basicMoveBy(int dx, int dy);

  
  public void changed() {
    invalidate();
    for (FigureChangeListener l : listenerList ) {
      FigureChangeEvent e = new FigureChangeEvent(this);
      l.figureChanged( e );
    }
  }

  public void addFigureChangeListener(FigureChangeListener l) {
    listenerList.add(l);
  }
  
  public void removeFigureChangeListener(FigureChangeListener l) {
    listenerList.remove(l);
  }
  
}
