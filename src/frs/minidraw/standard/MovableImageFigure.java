package frs.minidraw.standard;

import java.awt.Point;

public class MovableImageFigure extends ImageFigure{
	public MovableImageFigure(String imagename, Point p) {
		super(imagename, p);
	}
}