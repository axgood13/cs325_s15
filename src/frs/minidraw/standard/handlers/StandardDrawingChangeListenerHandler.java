package frs.minidraw.standard.handlers;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingChangeEvent;
import frs.minidraw.framework.DrawingChangeListener;
import frs.minidraw.framework.DrawingChangeListenerHandler;

/** The Subject role of the observer pattern for DrawingChangeListeners.

    You can use this default implementation in all objects that must
    handle DrawingChangeListeners.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

public class StandardDrawingChangeListenerHandler
  implements DrawingChangeListenerHandler {

  /** list over all associated listeners */
  protected List<DrawingChangeListener> fListeners;

  public StandardDrawingChangeListenerHandler() {
    fListeners = new ArrayList<DrawingChangeListener>();
  }

  /**
   * Adds a listener for this drawing.
   */
  public void addDrawingChangeListener(DrawingChangeListener listener) {
    fListeners.add(listener);
  }
  
  /**
   * Removes a listener from this drawing.
   */
  public void removeDrawingChangeListener(DrawingChangeListener listener) {
    fListeners.remove(listener);
  }

  /**
   * Fire a 'drawingInvalidated' event
   */
  public void fireDrawingInvalidated(Drawing source, Rectangle r) {
    for (DrawingChangeListener l : fListeners ) {
      l.drawingInvalidated(new DrawingChangeEvent(source, r));
    }
  }

  /**
   * Fire a 'drawingUpdate' event
   */
  public void fireDrawingRequestUpdate(Drawing source) {
    for (DrawingChangeListener l : fListeners ) {
      l.drawingRequestUpdate(new DrawingChangeEvent(source, null));
    }
  }
}
