package frs.minidraw.standard.handlers;

import java.util.ArrayList;
import java.util.List;

import frs.minidraw.framework.Figure;
import frs.minidraw.framework.SelectionHandler;

/** The standard selection handler contains default implementation
    of manageing a drawing's multiple figure selection mechanism.

    Responsibility
    A) Act as a collection of figures presently selected in a drawing.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

public class StandardSelectionHandler implements SelectionHandler {

  /** list of all figures currently selected */
  protected List<Figure> selectionList;
  
  public StandardSelectionHandler() {
    selectionList = new ArrayList<Figure>();
  }

  /**
   * Get an iterator over all selected figures
   */
  public List<Figure> selection() {
    return selectionList;
  }

  /**
   * Adds a figure to the current selection.
   */
  public void addToSelection(Figure figure) {
    if (! selectionList.contains(figure) ) {
      selectionList.add(figure);
      figure.changed();
    }
  }

  /**
   * Removes a figure from the selection.
   */
  public void removeFromSelection(Figure figure) {
    if ( selectionList.contains(figure) ) {
      selectionList.remove(figure);
      figure.changed();
    }
  }

  /**
   * If a figure isn't selected it is added to the selection.
   * Otherwise it is removed from the selection.
   */
  public void toggleSelection(Figure figure) {
    if (selectionList.contains(figure)) {
      removeFromSelection(figure);
    } else {
      addToSelection(figure);
      figure.changed();
    }
  }
  
  /**
   * Clears the current selection.
   */
  public void clearSelection() {
    for (Figure f: selectionList) {
      f.changed();
    }
    selectionList = new ArrayList<Figure>();
  }
}
