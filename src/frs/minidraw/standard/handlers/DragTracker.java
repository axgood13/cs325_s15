package frs.minidraw.standard.handlers;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.Figure;
import frs.minidraw.framework.Tool;
import frs.minidraw.standard.AbstractTool;

/** A DragTracker tool moves the set of figures defined by the drawing's
 * selection container (= the figures presently selected).

 * This code is partially copied from JHotDraw 5.1

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 
*/

public class DragTracker extends AbstractTool implements Tool {

  private Figure  figure;
  private int     fLastX, fLastY;      // previous mouse position

  public DragTracker(DrawingEditor editor, Figure figure) {
    super(editor);
    this.figure = figure;
    
  }

  public void mouseDown(MouseEvent e, int x, int y) {
    super.mouseDown(e, x, y);
    fLastX = x; fLastY = y;
    
    Drawing model = editor().drawing();
    
    if ( e.isShiftDown() ) {
      model.toggleSelection(figure);
    } else if ( ! model.selection().contains(figure) ) {
      model.clearSelection();
      model.addToSelection(figure);
    }
  }

  public void mouseDrag(MouseEvent e, int x, int y) {
    for ( Figure f : editor().drawing().selection() ) {
      f.moveBy( x - fLastX, y - fLastY );
    }
    fLastX = x;
    fLastY = y; 
  }

  public void keyDown(KeyEvent evt, int key) {
  }
}
