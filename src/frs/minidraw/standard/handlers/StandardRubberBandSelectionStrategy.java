package frs.minidraw.standard.handlers;

import java.awt.Rectangle;
import java.util.Iterator;

import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.Figure;
import frs.minidraw.framework.RubberBandSelectionStrategy;

/** The standard RubberBandSelection strategy that simply selects
    all figures within the rubber band rectangle.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

public class StandardRubberBandSelectionStrategy
  implements RubberBandSelectionStrategy {

  public void selectGroup(Drawing model, Rectangle rubberBandRectangle,
                          boolean toggle) {
    Iterator<Figure> i = model.iterator();
    while ( i.hasNext() ) {
      Figure figure = i.next();
      Rectangle r2 = figure.displayBox();
      if (rubberBandRectangle.contains(r2.x, r2.y) && 
          rubberBandRectangle.contains(r2.x+r2.width, r2.y+r2.height)) {
        if (toggle)
          model.toggleSelection(figure);
        else
          model.addToSelection(figure);
       }
    }
  }
}
