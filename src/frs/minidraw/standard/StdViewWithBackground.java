package frs.minidraw.standard;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import frs.minidraw.framework.DrawingEditor;

/** A drawing view that paints the background with a fixed image */

public class StdViewWithBackground extends StandardDrawingView {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
Image background;

 /** Create a DrawingView that features a graphical image as
      background for figures.
      @param backgroundName name of an image previously loaded by the
      image manager.
  */
  public StdViewWithBackground(DrawingEditor editor, String backgroundName) {
    super(editor);
    ImageManager im = ImageManager.getSingleton();
    this.background = im.getImage(backgroundName);
  }

  public StdViewWithBackground(DrawingEditor editor, Image background) {
    super(editor);
    this.background = background;
  }

   public void drawBackground(Graphics g) {
    g.drawImage(background, 0, 0, null );
  }
  
  public Dimension getPreferredSize() {
    return new Dimension( background.getWidth(null),
                          background.getHeight(null) );
  }

  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

}
