package frs.minidraw.standard;

import java.awt.Point;

public class ImmovableImageFigure extends ImageFigure{
	public ImmovableImageFigure(String imagename, Point p) {
		super(imagename, p);
	}
}
