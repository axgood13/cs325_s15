package frs.semimon;

import javax.swing.JTextField;

import frs.core.Board.Placement;
import frs.core.DiceRoller;
import frs.core.DoublesHandler;
import frs.core.HotgammonFactory;
import frs.core.MoveRules;
import frs.core.PlacementProvider;
import frs.core.TurnChanger;
import frs.core.WinnerDeterminer;
import frs.hotgammon.Game;
import frs.minidraw.framework.Drawing;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.DrawingView;
import frs.minidraw.standard.StandardDrawing;
import frs.minidraw.standard.StdViewWithBackground;
import frs.variants.dieRolls.RandomDiceRoller;
import frs.variants.doublesHandler.IgnoreDoublesHandler;
import frs.variants.moves.StdMoveValidator;
import frs.variants.placementProvider.StandardPlacementProvider;
import frs.variants.playerInTurn.AlternatingTurnChanger;
import frs.variants.winners.BearOffFirstDeterminer;

public class SemimonFactory implements HotgammonFactory{
	
	@Override
	public MoveRules createMoveRules(Game game) {
		return new StdMoveValidator(game);
	}

	@Override
	public WinnerDeterminer createWinnerDeterminer(Game game) {
		return new BearOffFirstDeterminer(game);
	}

	@Override
	public TurnChanger createTurnChanger(Game game) {
		return new AlternatingTurnChanger(game);
	}

	@Override
	public DiceRoller createDiceRoller() {
		return new RandomDiceRoller();
	}

	@Override
	public DoublesHandler createDoublesHandler() {
		return new IgnoreDoublesHandler();
	}
	
	public DrawingView createDrawingView( DrawingEditor editor ) {
	    DrawingView view = new StdViewWithBackground(editor, "board");
	    return view;
	}

	@Override
	public PlacementProvider createPlacementProvider() {
		return new StandardPlacementProvider();
	}

	@Override
	public void setPlacementArray(Placement[] placementArray) {
		
	}
	
	  public Drawing createDrawing( DrawingEditor editor ) {
		    return new StandardDrawing();
		  }

		  public JTextField createStatusField( DrawingEditor editor ) {
		    JTextField statusField = new JTextField( "Hello HotGammon..." );
		    statusField.setEditable(false);
		    return statusField;
		  }
}
