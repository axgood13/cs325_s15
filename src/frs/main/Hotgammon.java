package frs.main;

import java.awt.Point;

import frs.backgammon.BackgammonFactory;
import frs.backgammonWithCustomBoard.BackgammonWithCustomBoardFactory;
import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.HotgammonFactory;
import frs.core.PlacementProvider;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.Convert;
import frs.hotgammon.view.HotgammonDrawing;
import frs.hotgammon.view.HotgammonTool;
import frs.minidraw.framework.DrawingEditor;
import frs.minidraw.framework.Figure;
import frs.minidraw.standard.ImmovableImageFigure;
import frs.minidraw.standard.MiniDrawApplication;
import frs.minidraw.standard.MovableImageFigure;

public class Hotgammon {
	public static void main(String[] args) {
		HotgammonFactory factory = new BackgammonFactory(); //To test bearing off, change BackgammonFactory() to BackgammonWithCustomBoardFactory()
															 //and uncomment lines 27-38.
//		Placement[] placementArray = { 
//									new Placement(Location.R5, new frs.core.GameImpl.Point(Color.RED, 1)) ,
//									new Placement(Location.R4, new frs.core.GameImpl.Point(Color.RED, 1)) ,
//									new Placement(Location.R3, new frs.core.GameImpl.Point(Color.RED, 1)) ,
//									new Placement(Location.R6, new frs.core.GameImpl.Point(Color.BLACK, 2)) ,
//									new Placement(Location.R2, new frs.core.GameImpl.Point(Color.BLACK, 4)) ,
//									new Placement(Location.R_BAR, new frs.core.GameImpl.Point(Color.RED, 2)) ,
//									new Placement(Location.R1, new frs.core.GameImpl.Point(Color.BLACK, 5)) ,
//									new Placement(Location.B_BAR, new frs.core.GameImpl.Point(Color.BLACK, 2)) ,
//};
//		factory.setPlacementArray(placementArray);

		Game gameImpl = new GameImpl(factory);
		PlacementProvider placementProvider = factory.createPlacementProvider();
		
	    DrawingEditor editor = new MiniDrawApplication( "HOTGAMMON", factory);
		gameImpl.addGameObserver(new HotgammonDrawing(gameImpl, editor));
	    
	    editor.open();
	    
	    Figure redDie = new ImmovableImageFigure("die0", new Point(216, 202));
	    Figure blackDie = new ImmovableImageFigure("die0", new Point(306, 202));
	    
	    editor.drawing().add(redDie);
	    editor.drawing().add(blackDie);
	    
	    for (Placement placement : placementProvider.placementArray()) {
	    	if (placement.point.getPlayer() == Color.BLACK) {
	    		for (int i = 0; i < placement.point.getCount(); i++) {
	    			editor.drawing().add(new MovableImageFigure("blackchecker", Convert.locationAndCount2xy(placement.location, i)));
	    		}
	    	}
	    	else {
	    		for (int i = 0; i < placement.point.getCount(); i++) {
	    			editor.drawing().add(new MovableImageFigure("redchecker", Convert.locationAndCount2xy(placement.location, i)));
	    		}
	    	}
	    }
	    gameImpl.newGame();
	    HotgammonTool hotgammonTool = new HotgammonTool(editor, gameImpl);
	    editor.setTool(hotgammonTool.dieRollerTool());
	}
}
