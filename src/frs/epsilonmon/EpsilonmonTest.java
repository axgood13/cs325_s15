package frs.epsilonmon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.GameImpl.Point;
import frs.core.HotgammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

/**
 * Testing skeleton for AlphaMon.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Department of Computer Science Aarhus University
 * 
 * Please visit http://www.baerbak.com/ for further information.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class EpsilonmonTest {
	private Game game;
	private HotgammonFactory factory;

	@Before
	public void setup() {
		factory = new EpsilonmonFactory();
		game = new GameImpl(factory);
	}

	@Test
	public void shouldHaveNoPlayerInTurnAfterNewGame() {
		game.newGame();
		assertEquals(Color.NONE, game.getPlayerInTurn());
	}

	@Test
	public void shouldHaveTwoBlackCheckersOnR1() {
		Placement[] placementArray = {new Placement(Location.R1, new Point(Color.BLACK, 2))};
		factory.setPlacementArray(placementArray);	
		game.newGame();
		assertEquals(2, game.getCount(Location.R1));
	}

	@Test
	public void gameShouldEndAfterSixRolls() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertTrue(game.winner() != null && game.winner() != Color.NONE);
	}

	@Test
	public void noWinnerUntilGameIsOver() {
		game.newGame();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
		game.nextTurn();
		assertTrue(game.winner() == Color.RED);
		game.nextTurn();
		assertFalse(game.winner() == Color.RED);
	}

	@Test
	public void winnerIsAlwaysRed() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertFalse(game.winner() == Color.BLACK);
		assertFalse(game.winner() == Color.NONE);
		assertTrue(game.winner() == Color.RED);
	}
	
	@Test 
	public void noWinnerEverAfter6Rolls() {
		game.newGame();
		game.nextTurn();
		for (int i = 0; i < 10; i++) {
			game.nextTurn();
		}
		assertEquals(Color.NONE, game.winner());
	}
	
	@Test
	public void diceAlwaysGreaterThanZero() {
		game.newGame();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertTrue(roll[0] > 0);
		assertTrue(roll[1] > 0);
		for (int i = 0; i < 20; i++) {
			game.nextTurn();
			roll = game.diceThrown();
			assertTrue(roll[0] > 0);
			assertTrue(roll[1] > 0);
		}
	}
	
	@Test
	public void diceAlwaysLessThanSeven() {
		game.newGame();
		game.nextTurn();
		int[] roll = game.diceThrown();
		assertTrue(roll[0] < 7);
		assertTrue(roll[1] < 7);
		for (int i = 0; i < 20; i++) {
			game.nextTurn();
			roll = game.diceThrown();
			assertTrue(roll[0] < 7);
			assertTrue(roll[1] < 7);
		}
	}
}
