package frs.variants.moves;

import frs.core.Board;
import frs.core.GameImpl.Point;
import frs.core.MoveRules;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

public class StdMoveValidator implements MoveRules {
	Game game;
	public StdMoveValidator(Game game) {
		this.game = game;
	}
	@Override
	public boolean isValidMove(Location from, Location to) {
		if (playerHasPieceInBar(game.getPlayerInTurn())) {
			if ((game.getPlayerInTurn() == Color.BLACK && from != Location.B_BAR)
					|| (game.getPlayerInTurn() == Color.RED && from != Location.R_BAR)) {
				return false;
			}
		}
		if (moveIsInCorrectDirection(game.getPlayerInTurn(), Location.distance(from, to))
				&& moveIsCorrectDistance(from, to, game.diceValuesLeft())
				&& moveIsOnValidPlayerOrOneOpponent(from, to)) {
			if (to == Location.B_BEAR_OFF || to == Location.R_BEAR_OFF) {
				int redCount = getRedCount();
				int blackCount = getBlackCount();
				if (!playerHasAllPiecesHome(redCount, blackCount, Math.abs(Location.distance(from, to))) || !playerMakesCorrectBearOffMove(from, to)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private int getBlackCount() {
		int blackCount = 0;
		for (Location location : Location.values()) {
			blackCount += game.getColor(location) == Color.BLACK ? game.getCount(location) : 0;
		}
		return blackCount;
	}
	
	private int getRedCount() {
		int redCount = 0;
		for (Location location : Location.values()) {
			redCount += game.getColor(location) == Color.RED ? game.getCount(location) : 0;
		}
		return redCount;
	}
	
	private boolean playerHasAllPiecesHome(int redCount, int blackCount, int moveDistance) {
		Location[] redHomeLocations = {Location.R1, Location.R2, Location.R3, Location.R4, Location.R5, Location.R6, Location.R_BEAR_OFF};
		Location[] blackHomeLocations = {Location.B1, Location.B2, Location.B3, Location.B4, Location.B5, Location.B6, Location.B_BEAR_OFF};
		int count = 0;
		if (game.getPlayerInTurn() == Color.RED) {
			for (Location location : redHomeLocations) {
				count += game.getCount(location);
			}
			return count == redCount;
		}
		else {
			for (Location location : blackHomeLocations) {
				count += game.getCount(location);
			}
			return count == blackCount;
		}
	}
	
	private boolean playerMakesCorrectBearOffMove(Location from, Location to) {
		if (game.getPlayerInTurn() == Color.RED) {
			Location[] redHomeLocations = {Location.R6, Location.R5, Location.R4, Location.R3, Location.R2, Location.R1, Location.R_BEAR_OFF};
			Location redHighestLocation = null;
			for (Location location : redHomeLocations) {
				if (game.getCount(location) > 0) {
					redHighestLocation = location;
					break;
				}
			}
			if (from != redHighestLocation) {
				for (int die : game.diceValuesLeft()) {
					if (Math.abs(Location.distance(from, to)) == die) {
						return true;
					}
				}
				return false;
			}
		}
		else {
			Location[] blackHomeLocations = {Location.B6, Location.B5, Location.B4, Location.B3, Location.B2, Location.B1, Location.B_BEAR_OFF};
			Location blackHighestLocation = null;
			for (Location location : blackHomeLocations) {
				if (game.getCount(location) > 0) {
					blackHighestLocation = location;
					break;
				}
			}
			if (from != blackHighestLocation) {
				for (int die : game.diceValuesLeft()) {
					if (Math.abs(Location.distance(from, to)) == die) {
						return true;
					}
				}
				return false;
			}
		}
		return true;
	}

	private boolean playerHasPieceInBar(Color playerInTurn) {
		return playerInTurn == Color.BLACK
				&& game.getCount(Location.B_BAR) > 0
				|| playerInTurn == Color.RED
				&& game.getCount(Location.R_BAR) > 0;

	}

	public boolean moveIsInCorrectDirection(Color playerInTurn, int distance) {
		return distance / playerInTurn.getSign() > 0;
	}

	public boolean moveIsCorrectDistance(Location from, Location to,
			int[] diceValuesLeft) {
		int distance = Math.abs(Location.distance(from, to));
		boolean playerIsBearingOff = to == Location.R_BEAR_OFF
				|| to == Location.B_BEAR_OFF;
		for (int die : diceValuesLeft) {
			if (die == distance) {
				return true;
			} else if (playerIsBearingOff) {
				if (die >= distance) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean moveIsOnValidPlayerOrOneOpponent(Location from,
			Location to) {
		Color fromPlayer = game.getColor(from);
		Color toPlayer = game.getColor(to);
		return fromPlayer == toPlayer || toPlayer == Color.NONE
				|| game.getCount(to) == 1;
	}
	
	
}
