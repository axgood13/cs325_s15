package frs.variants.moves;

import frs.core.MoveRules;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

public class BasicMoveValidator implements MoveRules {
	Game game;
	public BasicMoveValidator(Game game) {
		this.game = game;
	}
	@Override
	public boolean isValidMove(Location from, Location to) {
		return game.getColor(to) != game.getPlayerInTurn() && game.getCount(to) > 0;
	}
}
