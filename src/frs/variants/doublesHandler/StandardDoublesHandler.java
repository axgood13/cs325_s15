package frs.variants.doublesHandler;

import frs.core.DoublesHandler;

public class StandardDoublesHandler implements DoublesHandler{

	@Override
	public int[] handleDoublesAndReturnAllDice(int[] diceThrown) {
		if (diceThrown[0] == diceThrown[1]) {
			return new int[] {diceThrown[0], diceThrown[0], diceThrown[0], diceThrown[0]};
		}
		else {
			return diceThrown;
		}
	}
}
