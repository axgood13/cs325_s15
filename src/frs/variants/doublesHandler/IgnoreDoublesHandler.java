package frs.variants.doublesHandler;

import frs.core.DoublesHandler;

public class IgnoreDoublesHandler implements DoublesHandler{

	@Override
	public int[] handleDoublesAndReturnAllDice(int[] diceThrown) {
		return diceThrown;
	}
}
