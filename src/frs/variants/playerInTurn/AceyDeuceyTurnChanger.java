package frs.variants.playerInTurn;

import frs.core.TurnChanger;
import frs.hotgammon.Game;

public class AceyDeuceyTurnChanger implements TurnChanger{
	private int[] diceThrown;
	private int[] lastDiceThrown;
	private Game gameImpl;
	
	public AceyDeuceyTurnChanger(Game gameImpl) {
		this.gameImpl = gameImpl;
	}
	
	public int changeTurns() {
		diceThrown = gameImpl.diceThrown();
		if (lastDiceThrownIsAceyDeucey()) {
			return gameImpl.getPlayerInTurn().getSign();
		}
		else {
			return -gameImpl.getPlayerInTurn().getSign();
		}
	}
	
	public boolean lastDiceThrownIsAceyDeucey() {
		return (lastDiceThrown[0] == 1 && lastDiceThrown[1] == 2) ||
			   (lastDiceThrown[0] == 2 && lastDiceThrown[1] == 1);
	}
	
	@Override
	public int signOfStartingPlayer() {
		diceThrown = gameImpl.diceThrown();
		return diceThrown[1] - diceThrown[0] / (Math.abs(diceThrown[1] - diceThrown[0]));
	}

	@Override
	public void setLastDiceThrown(int[] lastDiceThrown) {
		this.lastDiceThrown = lastDiceThrown;
	}
}
