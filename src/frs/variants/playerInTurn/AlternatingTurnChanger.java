package frs.variants.playerInTurn;

import frs.core.TurnChanger;
import frs.hotgammon.Game;

public class AlternatingTurnChanger implements TurnChanger{
	private int[] diceThrown;
	private Game gameImpl;
	
	public AlternatingTurnChanger(Game gameImpl) {
		this.gameImpl = gameImpl;
	}
	
	@Override
	public int changeTurns() {
		return -gameImpl.getPlayerInTurn().getSign();
	}
	
	@Override
	public int signOfStartingPlayer() {
		diceThrown = gameImpl.diceThrown();
		int numerator = diceThrown[1] - diceThrown[0];
		int denominator = Math.abs(diceThrown[1] - diceThrown[0]);
		int playerInTurnSign = numerator / denominator;
		return playerInTurnSign;
	}

	@Override
	public void setLastDiceThrown(int[] lastDiceThrown) {
	}
}
