package frs.variants.dieRolls;

import frs.core.DiceRoller;

public class RandomDiceRoller implements DiceRoller{
	public static int[] dieFaces = {1 ,2, 3, 4, 5, 6};

	@Override
	public int[] rollDice(int throwCount) {
		int[] pair = {(int) (Math.random() * dieFaces.length) + 1, (int) (Math.random() * dieFaces.length) + 1};
		return pair;
	}
}
