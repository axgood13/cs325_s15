package frs.variants.dieRolls;

import frs.core.DiceRoller;

public class SimpleIncrementingDiceRoller implements DiceRoller{
	public int[][] possibleThrows = { {1 , 2} , {3 , 4} , {5 , 6}};
	
	@Override
	public int[] rollDice(int throwCount) {
		return possibleThrows[throwCount % possibleThrows.length];
	}

}
