package frs.variants.placementProvider;

import frs.core.Board.Placement;
import frs.core.GameImpl.Point;
import frs.core.PlacementProvider;
import frs.hotgammon.Color;
import frs.hotgammon.Location;

public class StandardPlacementProvider implements PlacementProvider{

	@Override
	public Placement[] placementArray() {
		Placement[] placementArray = {	new Placement(Location.R1, new Point(Color.BLACK, 2)) ,
				new Placement(Location.B6, new Point(Color.BLACK, 5)) ,
				new Placement(Location.B8, new Point(Color.BLACK, 3)) ,
				new Placement(Location.R12, new Point(Color.BLACK, 5)) ,
				new Placement(Location.B1, new Point(Color.RED, 2)) ,
				new Placement(Location.R6, new Point(Color.RED, 5)) ,
				new Placement(Location.R8, new Point(Color.RED, 3)) ,
				new Placement(Location.B12, new Point(Color.RED, 5)) };
		return placementArray;
	}

}
