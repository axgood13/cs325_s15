package frs.variants.placementProvider;

import frs.core.Board.Placement;
import frs.core.GameImpl.Point;
import frs.core.PlacementProvider;
import frs.hotgammon.Color;
import frs.hotgammon.Location;

public class HypergammonPlacementProvider implements PlacementProvider{

	@Override
	public Placement[] placementArray() {
		Placement[] placementArray = {	new Placement(Location.R1, new Point(Color.BLACK, 1)) ,
				new Placement(Location.R2, new Point(Color.BLACK, 1)) ,
				new Placement(Location.R3, new Point(Color.BLACK, 1)) ,
				new Placement(Location.B1, new Point(Color.RED, 1)) ,
				new Placement(Location.B2, new Point(Color.RED, 1)) ,
				new Placement(Location.B3, new Point(Color.RED, 1))	};
		return placementArray;
	}

}
