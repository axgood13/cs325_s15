package frs.variants.winners;

import frs.core.WinnerDeterminer;
import frs.hotgammon.Color;
import frs.hotgammon.Game;

public class RedWinsAndSixRollsMaxDeterminer implements WinnerDeterminer{
	private final int NUMBER_OF_MOVES = 6;
	private int throwCount;
	
	@Override
	public Color winner() {
		if (throwCount != NUMBER_OF_MOVES) {
			return Color.NONE;
		}
		else {
			return Color.RED;
		}
	}

	@Override
	public void setThrowCount(int throwCount) {
		this.throwCount = throwCount;
	}
}
