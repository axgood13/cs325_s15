package frs.variants.winners;

import frs.core.WinnerDeterminer;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

public class BearOffFirstDeterminer implements WinnerDeterminer{
	Game game;
	
	public BearOffFirstDeterminer(Game game) {
		this.game = game;
	}

	@Override
	public Color winner() {
		if (redCount() != 0 && blackCount() != 0) {
			if (redBearOffCount() == redCount()) {
				return Color.RED;
			}
			else if(blackBearOffCount() == blackCount()) {
				return Color.BLACK;
			}
			else {
				return Color.NONE;
			}
		}
		else {
			return Color.NONE;
		}
	}

	private int blackBearOffCount() {
		return game.getCount(Location.B_BEAR_OFF);
	}

	private int redBearOffCount() {
		return game.getCount(Location.R_BEAR_OFF);
	}

	private int blackCount() {
		int blackCount = 0;
		for (Location location : Location.values()) {
			blackCount += game.getColor(location) == Color.BLACK ? game.getCount(location) : 0;
		}
		return blackCount;
	}

	private int redCount() {
		int redCount = 0;
		for (Location location : Location.values()) {
			redCount += game.getColor(location) == Color.RED ? game.getCount(location) : 0;
		}
		return redCount;
	}

	@Override
	public void setThrowCount(int throwCount) {
	}
}
